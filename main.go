package main

import (
	"ait_cyclon/gossip"
	"ait_cyclon/gossip/activity_detection"
	//"ait_cyclon/gossip/cyclon"
	"ait_cyclon/monitoring"
	"ait_cyclon/network"
	"ait_cyclon/session"
	"encoding/gob"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"sync"
	"time"
)

func init() {
	// rand is deterministic until you seed it
	rand.Seed(time.Now().UnixNano())
}

var wg sync.WaitGroup

func main() {
	var netAddr, sequenceJsonPath, myID string
	var /*monitorCy,*/ monitorNetwork, monitorSession, monitorAd bool
	var sessionPushFanout int

	// For ReactiveActivityGossiper
	var activityDetectionFanout int

	cwd, _ := os.Getwd()

	flag.StringVar(&netAddr, "net", "",
		"Address of the network in CIDR format (x.y.z.t/m); defaults to looking for an interface w/ MTU of 1450;")
	// Directories
	flag.StringVar(&monitoring.MonitoringDir, "m", cwd,
		"Monitoring output directory; defaults to current working dir;")
	flag.StringVar(&sequenceJsonPath, "sequence_json", cwd,
		"Path to the JSON file containing the initial sequence;")
	flag.StringVar(&myID, "name", "",
		"My name (defaults to random string);")
	// Monitor?
	flag.BoolVar(&monitorNetwork, "mnet", false, "Output monitoring CSVs about the network traffic?")
	//flag.BoolVar(&monitorCy, "mcy", false, "Output monitoring CSVs about Cyclon?")
	flag.BoolVar(&monitorSession, "msession", false, "Output monitoring CSVs about sessions?")
	flag.BoolVar(&monitorAd, "mactivity", false, "Output monitoring CSVs about Activity Detection?")
	// Periods
	flag.IntVar(&sessionPushFanout, "session_push_fanout", 2, "Number of peers to send new session to.")

	// ReactiveActivityGossiper stuff
	flag.IntVar(&activityDetectionFanout, "ad_fanout", 2, "Fanout of the Activity Detection's gossip broadcast.")

	flag.Parse()

	myAddr := ""
	var err error
	if myAddr, err = network.GetMyAddress(netAddr); err != nil {
		log.Fatalf("[main] Error getting my address: %v\n", err)
	}

	var myself gossip.Descriptor = gossip.NewBaseDescriptor(myID, myAddr)
	fmt.Printf("I am: %+v\n\n", myself)

	// Create my dear old UDP network manager for gossiping
	// var udpManager *network.UDPManager
	// if udpManager, err = network.CreateUDPManager(MyAddr); err != nil {
	// 	log.Fatalf("[main] Error initialising UDP network manager: %v\n", err)
	// }
	var tcpManager *network.TCPManager
	if tcpManager, err = network.CreateTCPManager(myAddr, monitorNetwork); err != nil {
		log.Fatalf("[main] Error initialising TCP network manager: %v\n", err)
	}

	if monitorNetwork {
		tcpMonitor := new(monitoring.Monitor)
		tcpMonitor.Init(tcpManager)
		go tcpMonitor.Loop()
	}

	// var aitNm *ait.NetworkManager
	// if aitNm, err = ait.CreateNetworkManager(MyAddr, myself.(*ait.TimestampedDescriptor)); err != nil {
	// 	log.Fatalf("[main] Error initialising AIT TCP/HTTP network: %v\n", err)
	// }

	gob.Register(&gossip.BaseDescriptor{})
	//gob.Register(&session.TimestampedDescriptor{})
	gob.Register(gossip.Payload{})
	//gob.Register(activity_detection.Sequence{})
	gob.Register(activity_detection.Payload{})
	gob.Register(session.SessionMessage{})

	// var ag *activity_detection.ActivityGossiper
	// ag, err = activity_detection.NewActivityGossiper(myself, tcpManager, cy, sequenceJsonPath,
	// 	activityDetectionPeriod, activityDetectionSetSize, activityDetectionGossipSize)
	// if err != nil {
	// 	log.Fatalf("[main] Error creating ActivityGossiper: %v\n", err)
	// }
	// if monitorAd {
	// 	adMonitor := new(monitoring.Monitor)
	// 	adMonitor.Init(ag)
	// 	go adMonitor.Loop()
	// }
	// go ag.Loop()

	var rag *activity_detection.ReactiveActivityGossiper
	rag, err = activity_detection.NewReactiveActivityGossiper(myself, tcpManager,
		activityDetectionFanout, sequenceJsonPath, monitorAd)
	if err != nil {
		log.Fatalf("[main] Error creating ReactiveActivityGossiper: %v\n", err)
	}
	if monitorAd {
		radMonitor := new(monitoring.Monitor)
		radMonitor.Init(rag)
		go radMonitor.Loop()
	}
	wg.Add(1)
	go rag.Loop()

	// sm := ait.NewSessionManager(cy, aitNm, myself.(*ait.TimestampedDescriptor))
	sm := session.NewSessionManager(tcpManager, rag, myself,
		monitorSession, sessionPushFanout)
	wg.Add(1)

	if monitorSession {
		aitMonitor := new(monitoring.Monitor)
		aitMonitor.Init(sm)
		go aitMonitor.Loop()
	}

	wg.Wait()
}
