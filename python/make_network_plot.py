#!/usr/bin/env python3

# Imports 
import os, re, argparse
import numpy as np
import pandas as pd
from plotly.offline import plot
import plotly.graph_objs as go

# CSV prefix
FILE_EXPRESSION="udpnetwork"
# default project dir
PROJECT_DIR=os.getcwd()
CSV_SUBDIR="/monitoring_output/"
PLOTS_SUBDIR="/plots/"

# Parse cmd line parameters
parser = argparse.ArgumentParser(
	description='Make plots from AIT\'s UDP network output.',
	formatter_class=argparse.ArgumentDefaultsHelpFormatter)
# We want the project dir, and except /monitoring_output as the CSV dir
parser.add_argument('--project_dir', '-p', metavar='dir', type=str, nargs='?', 
	help="Location of the AIT project", default=PROJECT_DIR)
parser.add_argument('--csv_dir', '-c', metavar='subdir', type=str, nargs='?', 
	help="Subdirectory containing the CSVs", default=CSV_SUBDIR)

args = parser.parse_args()


# Retrieving the CSVs
try:
	os.chdir(PROJECT_DIR+CSV_SUBDIR)
except:
	print('Could not enter {}, we abort.'.format(PROJECT_DIR+CSV_SUBDIR))
	exit(1)

df = pd.DataFrame()
for csv in os.listdir():
    if re.search(FILE_EXPRESSION, csv):
        #print("Reading file:", csv)
        csv_df=pd.read_csv(csv, parse_dates=['Timestamp'], index_col='Timestamp')
        if( df.empty ):
            df = csv_df
        else:
            df = df.append(csv_df,)
            
if df.empty:
	print("Our dataframe is empty (something went wrong), we abort.")
	exit(1)

df.sort_index(inplace=True)

# all columns: Address,bytesRec,nRec,bytesSent,nSent
# We study only two: bytesRec,nRec
columns=df.columns[1:3]

time_index=df.index.unique()

print("Successfully retrieved '{}' CSVs from {}.".format(
	FILE_EXPRESSION, PROJECT_DIR))
print("Retrieved", df.shape[0], "entries.")
print("The experiment has been running for {}.".format(
	df.index.max()-df.index.min()))

# Creating the results dataframe
results_df=pd.DataFrame(index=time_index)
results_df["nNodes"]=df[columns[0]].groupby(df.index).count()
for col in columns:
    results_df[col+"_mean"]=df[col].groupby(df.index).mean()
    results_df[col+"_std"]=df[col].groupby(df.index).std()

# Creating a plots subdirectory
PLOTS_DIR=PROJECT_DIR+PLOTS_SUBDIR

try:
    os.mkdir(PLOTS_DIR)
except FileExistsError:
    for f in os.listdir(PLOTS_DIR):
        if os.path.isfile(f):
            os.remove(PLOTS_DIR+f)
    pass
os.chdir(PLOTS_DIR)

print("We will write plots in {}".format(PLOTS_DIR))

# Creating the graph
data=[]

for col in columns:
    yaxis="y" #Bytes axis
    if col[0] == 'n':
        yaxis="y2"
        
    data.append(go.Scatter(
        name=col,
        x=time_index,
        y=results_df[col+"_mean"],
        error_y=dict(
            type='data',
            array=results_df[col+"_std"],
            visible=True,
            thickness=1,
            width=2,
            opacity=0.5
        ),
        mode="lines+markers",
        line = dict(
            width = 1.5),
        marker=dict(size=3),
        yaxis=yaxis
    ))

data.append(go.Scatter(
    x=time_index,
    y=results_df['nNodes'],
    mode="lines+markers",
    name="Number of nodes",
    line = dict(
        width = 0.5),
    marker=dict(size=2),
    yaxis='y2'
))

layout=dict(
    title="UDP traffic of AIT with {} nodes".format(results_df['nNodes'].max()),
    xaxis=dict(
        title='Timestamp'
    ),
    yaxis2=dict(
        #overlaying="y",
        side='right',
        #autorange=True,
        title='Quantity',
        domain=[0.5,1],
        #anchor="x",
        #position=0.55
    ),
    yaxis=dict(
        title='Bytes',
        domain=[0,0.5],
        anchor="x"
    ),
)
fig=go.Figure(data=data, layout=layout)
plot(fig, filename=PLOTS_DIR+"udp_traffic.html")

# If you want global data
print()
print("Global results:")
for col in columns:
	print(col)
	print("\tmean: {}".format(df[col].mean()))
	print("\tstd:  {}".format(df[col].std()))


# Be aware it's super simple to make a CSV out of this data:
#
# RPS period: Random Peer Sampling period; you would have to pass it as an argument
# to this script because it's not in the CSVs
# new_df=pd.Dataframe(columns=["RPS period", "nRec_mean", "nRec_std",
# 	"bytesRec_mean", "bytesRec_std"])
# new_df.append([300, X, Y, Z, T])
# # Add the others...
# And seve:
# new_df.to_csv('lalala.csv')
