#!/usr/bin/env python3

import docker
import subprocess as sp


# ---------- Setup ----------

client = docker.from_env()
low_level_client = docker.APIClient()


# ---------- Docker handles ----------

def build_project(build_script_location):
    try:
        print(sp.check_output(build_script_location,
                              stderr=sp.STDOUT).decode('utf-8'))
    except sp.CalledProcessError as e:
        print(e.output.decode('utf-8'))
        raise


def create_network(network):
    global client

    nets_list = client.networks.list(names=[network['name']])
    if len(nets_list) > 0:
        for net in nets_list:
            net.remove()
    print("Creating network {} with address {}.".format(
        network['name'], network['addr']))
    return client.networks.create(network['name'], driver="bridge",
                                  ipam=docker.types.IPAMConfig(pool_configs=[docker.types.IPAMPool(
                                      subnet=network['addr'])]))


def remove_containers(image_name=None, container_label=None, only_stop=False):
    global client

    if image_name is None:
        raise ValueError(
            "Please specify at least an ancestor image_name to remove containers.")

    print("remove_containers: image_name={}, container_label={}".format(
        image_name, container_label))

    cont_list = []
    if container_label is None:
        cont_list = client.containers.list(
            filters=dict(ancestor=image_name), all=True)
        if len(cont_list) == 0:
            return
        print("Removing {} existing containers having ancestor_image={}...".format(
            len(cont_list), image_name))
    else:
        cont_list = client.containers.list(
            filters=dict(label=container_label), all=True)
        if len(cont_list) == 0:
            print('Found no container having this image name and label.')
            return
        print("Removing {} existing containers having ancestor={} and label={}...".format(
            len(cont_list), image_name, container_label))
    for cont in cont_list:
        if only_stop:
            cont.stop()
        else:
            cont.remove(force=True)
    print("Successfully removed containers.")


def start_container(cont_name, cont_args, labels, network, image_name, volumes=None, cont_ip=None):
    global low_level_client, client

    # Register us to net_name with predefined IP cont_ip
    network_config = {}
    if cont_ip is not None:
        network_config = low_level_client.create_networking_config({
            network['name']: low_level_client.create_endpoint_config(ipv4_address=cont_ip)
        })

    # Create volumes configuration from the volumes dict
    volumes_config = {}
    volumes_cont_list = []
    if volumes is not None:
        for _, vol in volumes.items():
            volumes_config[vol['host_dir']] = {
                'bind': vol['cont_dir'], 'mode': vol['mode']}
            volumes_cont_list.append(vol['cont_dir'])

    host_config = low_level_client.create_host_config(binds=volumes_config)

    cont = low_level_client.create_container(image_name, cont_args, detach=True, volumes=volumes_cont_list,
                                             name=cont_name, labels=labels, networking_config=network_config, host_config=host_config)
    low_level_client.start(cont)

    cont_id = cont['Id'][:12]
    cont_ip = client.containers.get(cont_id).attrs['NetworkSettings'][
        'Networks'][network['name']]['IPAddress']

    return cont_id, cont_ip
