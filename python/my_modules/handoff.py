#!/usr/bin/env python3

import numpy as np
import pandas as pd
import copy
import random
from . import evaluation

DEBUG = False

# ---------- Probabilities ----------


def normalise_probability(P):
    Psum = np.sum(list(P.values()))
    if Psum == 0:
        return P
    else:
        return {k: v / Psum for k, v in P.items()}

# Returns the probability of using each device after d according to the
# Sequence S, with additive smoothing
SMOOTHING = 0.1


def get_transition_probability(S, d, apply_smoothing=False, smoothing=SMOOTHING):
    addresses = np.unique(S['Address'])
    if apply_smoothing:
        # we remove d from the considered addresses
        #addr=addresses[addresses != d]
        # The keys of P are the addresses of the nodes
        P = {key: smoothing for key in addresses}
        denom = smoothing*len(P)

        for i in range(len(S)-1):
            if S['Address'].iloc[i] == d:
                P[S['Address'].iloc[i+1]] += 1
                denom += 1

        P = {k: v/denom for k, v in P.items()}
        return P
    else:
        # The keys of P are the addresses of the nodes
        P = {key: 0 for key in addresses}

        # Count transitions in S
        for i in range(len(S)-1):
            if S['Address'].iloc[i] == d:
                # if len(S) == 228 and DEBUG:
                # print("#{}: {} -> {}".format(i, d, S['Address'].iloc[i+1]))
                P[S['Address'].iloc[i+1]] += 1

        # Normalise the vector
        return normalise_probability(P)


def prune_negligible_probabilities(P, threshold):
    P_pruned = {}
    for k, v in P.items():
        if v > threshold:
            P_pruned[k] = v
        else:
            P_pruned[k] = 0

    # Normalise the vector
    return normalise_probability(P_pruned)


def is_constant_probability(P):
    #assert(np.sum(list(P.values())) == 1)
    return np.count_nonzero(list(P.values())) == 1


def is_uniform_probability(P):
    #assert(np.sum(list(P.values())) == 1)
    n_positive_devices = np.count_nonzero(list(P.values()))
    # Needed if current_device removed from P
    if n_positive_devices == 0:
        return True
    if n_positive_devices == 1:
        return False

    uniform_list = [1/n_positive_devices]*n_positive_devices

    return np.allclose([v for k, v in P.items() if v > 0], uniform_list)


# ---------- Cost function ----------

# returns C_W, C_N
def compute_cost(W, current_device, next_device):
    # C = sum(W) + Ws - 2Wc - Wmyself
    # Wc = data chunks sent of chosen (next) device
    # we consider that the session weights Ws=1 .

    Wsum = np.sum(list(W.values()))
    # We don't have Wmyself anymore
    #Wmyself=W[current_device] if current_device in W else 0

    if current_device == next_device:
        # Waiting cost = 0
        # Net cost = Wsum
        # =>
        return 0, Wsum

    Wc = W[next_device] if next_device in W else 0

    return 1 - Wc,  Wsum


def compute_cost_expected_value(P, W, current_device):
    expected = 0
    Wsum = np.sum(list(W.values()))
    Wmyself = P[current_device] if current_device in P else 0

    for d, Wd in W.items():
        if d in P and P[d] != 0:
            # cost if d is chosen
            c_d = 0
            if d == current_device:
                c_d = Wsum - Wmyself
            else:
                # cost = sum(W) + w_session (=1) - 2 * w_chosen
                c_d = Wsum + 1 - 2 * Wd - Wmyself

            expected += P[d]*c_d

    return expected

# ---------- Reactive Session Handoff ----------


def reactive_session_handoff(activity_df, sequence_df, experiment_id,
                             current_round, next_round, W=None):

    ret_string = ""

    # If we stay on the same device, no reactive needed (it works)
    if current_round['Address'] == next_round['Address']:
        ret_string = "same device"

    next_device_sequence = evaluation.get_local_sequence(
        sequence_df, activity_df, experiment_id,
        next_round['Address'], next_round['Timestamp']+pd.Timedelta("1ms"))

    curr_round_in_next_seq = (
        next_device_sequence['Timestamp'] == current_round['Timestamp']).any()

    if curr_round_in_next_seq:
        # If the next device knows which device was just used, it can fetch the session
        # if DEBUG:
        #     print("[Reactive] next_device knows current_device: success")
        if ret_string == "":
            ret_string = "in sequence"
        else:
            ret_string += "|in sequence"
    if W is not None and \
            next_round['Address'] in W and W[next_round['Address']] > 0:
        # W is the dict of session chunks sent from the current device to the next ones
        # If current_device sent chunks to next_device, next_device directly asks to the former
        # if DEBUG:
        #     print("[Reactive] current_device sent session chunks to next_device: success")
        if ret_string == "":
            ret_string = "received chunks"
        else:
            ret_string += "|received chunks"

    if ret_string == "":
        last_device_in_sequence = evaluation.get_local_sequence(
            sequence_df, activity_df, experiment_id,
            next_round['Address'], next_round['Timestamp']).iloc[-1]['Address']

        # Else, we check last_device_in_sequence's own sequence
        last_device_in_sequence_bis = evaluation.get_local_sequence(
            sequence_df, activity_df, experiment_id,
            last_device_in_sequence, next_round['Timestamp']).iloc[-1]['Address']

        # If last_device_in_sequence knows which device was just used, we can
        # fetch the session
        if last_device_in_sequence_bis == current_round['Address']:
            # if DEBUG:
            #     print("[Reactive] next_device's correspondant knows current_device: success")
            if ret_string == "":
                ret_string = "redirected"
            else:
                ret_string += "|redirected"
        else:
            ret_string = "failed"
            # if DEBUG:
            #     print("[Reactive] Fail :(")

    return ret_string

# ---------- Session handoff strategy ----------


VARIANCE_FACTOR = 0
MIN_PROBA = 0
W_TOT = 1


def send_session_by_case_draft(P, current_round, next_round,
                               experiment_id, activity_df, sequence_df, result_series, gamma=1):
    # We make a copy of P
    #P = copy.deepcopy(P)
    # To remove d (on which the session takes place) from the considered next devices
    # del P[current_round['Address']]
    # # Normalise
    # P = normalise_probability(P)

    if current_round['Address'] in P:
        del P[current_round['Address']]
    other_devices = list(np.unique(list(P.keys())))
    n_devices = len(other_devices)

    # # If constant proba, return P
    # if is_constant_probability(P):
    #     if DEBUG:
    #         print("Constant!")
    #     W=P
    #     #return P

    # # If the proba is uniform, send nothing
    # if is_uniform_probability(P):
    #     if DEBUG:
    #         print("Uniform!")
    #     #return {k: min(1, v * Wtot) for k, v in P.items()}
    #     W={}
    #     #return {}

    P_pruned = prune_negligible_probabilities(P, 1/n_devices)

    W_uniform = {d: min(1, gamma / n_devices) for d in other_devices}
    W_pruned = {d: min(1, gamma * p) for d, p in P_pruned.items()}

    # result_series['ExpectedCost']=compute_cost_expected_value(P, W, current_round['Address'])
    # result_series['Cost']=compute_cost(W, current_round['Address'], next_round['Address'])
    # result_series['Reactive']=reactive_session_handoff(
    # activity_df, sequence_df, experiment_id, current_round, next_round, W)

    #result_series['ExpectedCostPruned']=compute_cost_expected_value(P, W_pruned, current_round['Address'])

    result_series['gamma'] = gamma
    result_series['next_devices'] = n_devices
    result_series['C_W_uniform'], result_series['C_N_uniform'] =\
        compute_cost(W_uniform, current_round[
                     'Address'], next_round['Address'])
    result_series['C_W'], result_series['C_N'] =\
        compute_cost(W_pruned, current_round['Address'], next_round['Address'])

    result_series['Reactive'] = reactive_session_handoff(
        activity_df, sequence_df, experiment_id, current_round, next_round, W_pruned)

    if DEBUG:
        print("\n[{} -> {} (gamma={})]\nP: {}\nP_pruned: {}".format(
            current_round['Address'], next_round['Address'], gamma,
            {k: v for k, v in P.items() if v != 0},
            {k: v for k, v in P_pruned.items() if v != 0}))
        #print("W_uniform: {}".format(W_uniform))
        print("W_pruned: {}".format({k: v for k, v in W_pruned.items() if v != 0}))
        # print("Uniform; C_W={}, C_N={}".format(result_series[
        #      'C_W_uniform'], result_series['C_N_uniform']))
        print("Pruned; C_W={}, C_N={}".format(
            result_series['C_W'], result_series['C_N']))
        print("Reactive='{}'".format(result_series['Reactive']))
        # print("Cost's expected value for W:", compute_cost_expected_value(P, P, current_round['Address']))
        # print("Cost's expected value for W_pruned:", compute_cost_expected_value(P,
        #     W_pruned, current_round['Address']))
        # print("Cost:{}\tCostPruned:{}\n".format(result_series['Cost'], result_series['CostPruned']))

    return result_series

    # W_pruned = prune_negligible_probabilities(P, 1/n_devices)


def send_session_by_case(P, d,
                         Wtot=W_TOT, variance_factor=VARIANCE_FACTOR, min_proba_factor=MIN_PROBA):
    # We make a copy of P
    P = copy.deepcopy(P)
    # To remove d (on which the session takes place) from the considered next
    # devices
    del P[d]
    # Normalise
    P = normalise_probability(P)

    if DEBUG:
        print("[send_session_by_case d={}, v={}, mpf={}]".format(
            d, variance_factor, min_proba_factor))
        print("P: {}".format(P))

    # If constant proba, return P
    if is_constant_probability(P):
        if DEBUG:
            print("Constant!\n")
        return P

    # If the proba is uniform, send nothing
    if is_uniform_probability(P):
        if DEBUG:
            print("Uniform!\n")
        # return {k: min(1, v * Wtot) for k, v in P.items()}
        return {}

    # Prune too small entries
    n_devices = len(P)
    P = {k: v for k, v in P.items()
         # We only keep devices whose proba is superior to min_proba_factor *
         # mean (1/N)
         if v > min_proba_factor / n_devices}
    # Normalise
    Psum = np.sum(list(P.values()))
    P = {k: v / Psum for k, v in P.items()}

    n_positive_devices = len(P)
    Pmean = 1/n_positive_devices

    Pmin = np.min(list(P.values()))
    Pmax = np.max(list(P.values()))

    max_variance_factor = min(
        Pmin / np.abs(Pmin - Pmean),  # Max value to have a non-negative Wmin
        (1 - Pmax) / (Pmax - Pmean))  # Max value to have Wmax <= 1
    if variance_factor > max_variance_factor:
        if DEBUG:
            print("[send_session_by_case d={}] variance_factor={} exceeds max_variance_factor={}".format(
                d, variance_factor, max_variance_factor))
        variance_factor = max_variance_factor

    if DEBUG:
        print("n_pos={}, Pmin={}, Pmax={}, Pmean={}, variance_factor={}".format(
            n_positive_devices, Pmin, Pmax, Pmean, variance_factor))

    W = {}

    # W = P + variance_factor * (P - Pmean)
    for i, p_i in P.items():
        W[i] = min(1,
                   Wtot * (p_i + variance_factor * (p_i - Pmean)))
    if DEBUG:
        print("W: {} sums to: {}\n".format(W, np.sum(list(W.values()))))

    return W


def send_session_to_n_most_likely(P, n, d):
    # We make a copy of P
    P = copy.deepcopy(P)
    # To remove d (on which the session takes place) from the considered next
    # devices
    del P[d]

    if n == len(P):
        return P

    W = {k: 0 for k in P.keys()}
    chosen_keys = []

    P2 = copy.deepcopy(P)
    while len(chosen_keys) < n:
        maxval = np.max(list(P2.values()))
        keys = [k for k, v in P.items() if v == maxval]

        for k in keys:
            del P2[k]

        if len(chosen_keys) + len(keys) <= n:
            chosen_keys.extend(keys)
        else:
            random.shuffle(keys)
            chosen_keys.extend(keys[:n - (len(chosen_keys) + len(keys))])

    Ptot = np.sum([P[k] for k in chosen_keys])

    for k in chosen_keys:
        W[k] = P[k] / Ptot

    return W


def send_session_uniformly_to_n_random_nodes(P, n, d):
    W = {k: 0 for k in P.keys() if k != d}

    p = 1 / len(W)

    for k in np.random.choice(list(W.keys()), n, replace=False):
        W[k] = p

    return W
