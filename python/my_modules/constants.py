#!/usr/bin/env python3

SESSION_FILE_EXPRESSION = "session"
ACTIVITY_FILE_EXPRESSION = "activity_gossiper"
NETWORK_FILE_EXPRESSION = "tcpnetwork"

SEQUENCE_FN = "sequence.csv"
MODEL_FN = "model.npy"
PARAMETERS_FN = "parameters.csv"
