#!/usr/bin/env python3

from . import model, docker_utils
from .constants import *
import datetime
import json
import numpy as np
import os
import pandas as pd
import random
import requests
import string
import sys
import time

# The following functions rely heavily on the experiment_parameters dictionary
# that contains every parameter of a given experiment
# Here is an example:
'''
experiment_parameters=dict(
    experiment_name="reactive",
    net=dict(
        name="ait_network{experiment_id}",
        addr="13.{experiment_id}.0.0/20",
        gossip_port=10337,
        rest_port=10338,
        rest_url="/session"
    ),
    dirs=dict(
        root=os.getenv('HOME')+'/Thèse/ait_cyclon/',
        json_subdir="json/",
        output_subdir="experimental_output/{experiment_name}/experiment{experiment_id}/"
    ),
    build_script_path="scripts/build_ait_cyclon.sh",
    
    image_name="ait_cyclon_image",
    container=dict(
        name="ait_exp{experiment_id}_{container_id}",
        label="experiment{experiment_id}",
        ip="13.{experiment_id}.0.{container_id}",
        only_stop=False,
        arguments="-net={net_addr} -m={monitor_dir} -sequence_json={json_path} \
        -session_period={session_period} -ad_setsize={activity_setsize} -ad_fanout={activity_fanout} \
        -msession -mactivity -bs_addr={bs_addr} "
    ),
    
    model_type="sparse",
    model_generation_parameters=dict(),
    
    sequence=dict(
        initial_length=20,
        total_length=100,
    ),
    
    n_devices=12,
    n_experiments=2,
    
    payload_size=1000,
    requests_interval=1,# s
    
    program_parameters=dict(
        session_period=2000, #ms
        #session_period=session_period, #ms
        activity_setsize=2, activity_fanout=3,
        #cy_period=int(REQUEST_INTERVAL_MEAN*1000), #ms
        #cy_viewsize=6, cy_gossipsize=2,
    )
)
'''

# ---------- Model and Sequence ----------


def generate_devices_sequence(experiment_parameters):
    # The model
    W = model.generate_device_use_model(experiment_parameters['n_devices'],
                                        experiment_parameters['model_type'],
                                        **experiment_parameters['model_generation_parameters'])
    S = model.random_walk(W, experiment_parameters['sequence']['total_length'])

    return W, S

# ---------- Dumping data ----------


def write_sequence_json(S_init, devices_name, devices_id, folder, fn):
    initial_sequence_length = len(S_init)
    serializable_S = [None]*initial_sequence_length
    past_times = [None]*initial_sequence_length
    now = datetime.datetime.utcnow()

    for i, s in enumerate(S_init):
        # We craft fake timestamps in increasing order
        s_time = now - datetime.timedelta(seconds=initial_sequence_length - i)

        serializable_S[i] = dict(
            Name=devices_name[s],
            Addr=devices_id[s],
            StartTime=s_time.strftime("%Y-%m-%d %H:%M:%S.%f")
        )

        past_times[i] = s_time

    try:
        os.mkdir(folder)
    except:
        pass

    fd = open(folder+fn, 'w')
    json.dump(serializable_S, fd)
    print("Wrote initial sequence to", folder+fn)

    return past_times


# ---------- The Experiment ----------

def bootstrap_experiment(experiment_id, experiment_parameters):
    network = dict(
        name=experiment_parameters['net']['name'].format(
            experiment_id=experiment_id),
        addr=experiment_parameters['net']['addr'].format(
            experiment_id=experiment_id)
    )
    output_dir = experiment_parameters['dirs']['output_subdir'].format(
        experiment_name=experiment_parameters['experiment_name'])
    output_dir += "experiment{}/".format(experiment_id)
    volumes = {
        'monitor': {
            'host_dir': experiment_parameters['dirs']['root']+output_dir,
            'cont_dir': "/"+output_dir,
            'mode': 'rw'
        },
        'json': {
            'host_dir': experiment_parameters['dirs']['root']+experiment_parameters['dirs']['json_subdir'],
            'cont_dir': "/"+experiment_parameters['dirs']['json_subdir'],
            'mode': 'ro'
        }
    }
    label = experiment_parameters['container']['label'].format(
        experiment_id=experiment_id)
    bs_addr = ""

    print("[Experiment #{}] Bootstrapping containers...".format(experiment_id))

    docker_utils.remove_containers(
        image_name=experiment_parameters['image_name'],
        container_label=experiment_parameters['container']['label'].format(
            experiment_id=experiment_id),
        only_stop=False)
    docker_utils.create_network(network)

    # We predefine the IPs
    if experiment_parameters['n_devices'] > 253:
        print("n_devices is too large, please make it below 253.")
        return

    containers_ip = [None]*experiment_parameters['n_devices']
    containers_name = [None]*experiment_parameters['n_devices']
    for i in range(experiment_parameters['n_devices']):
        containers_ip[i] = experiment_parameters['container']['ip'].format(
            experiment_id=experiment_id,
            container_id=i+2)
        containers_name[i] = experiment_parameters['container']['name'].format(
            experiment_id=experiment_id,
            container_id=i)

    W, S = generate_devices_sequence(experiment_parameters)

    json_fn = "experiment{}.json".format(experiment_id)
    past_times = write_sequence_json(
        S[:experiment_parameters['sequence']['initial_length']],
        containers_name, containers_ip,
        volumes['json']['host_dir'], json_fn)

    for device_id in range(experiment_parameters['n_devices']):
        cont_args = experiment_parameters['container']['arguments'].format(
            net_addr=network['addr'],
            monitor_dir=volumes['monitor']['cont_dir'],
            json_path=volumes['json']['cont_dir']+json_fn,
            name=containers_name[device_id], bs_addr=bs_addr,
            **experiment_parameters['program_parameters'])

        cont_id, cont_ip = docker_utils.start_container(
            containers_name[device_id], cont_args, [label], network,
            experiment_parameters['image_name'], volumes,
            cont_ip=containers_ip[device_id])

        # We use the first IP as a bootstrap address for the next containers
        if device_id == 0:
            bs_addr = containers_ip[device_id]+":" + \
                str(experiment_parameters['net']['gossip_port'])

        print("[Experiment #{} bootstrap {}/{}] Started container {} and IP {}".format(
            experiment_id, device_id+1, experiment_parameters['n_devices'],
            containers_name[device_id], cont_ip))

    print("[Experiment #{}] Finished containers bootstrap.\n".format(experiment_id))

    return containers_name, containers_ip, W, S, past_times


def conduct_experiment(experiment_id, experiment_parameters):

    sequence_df = pd.DataFrame(columns=["Address", "Timestamp"])

    # provide requests settings once and for all here
    s = requests.Session()
    s.headers.update({
        "Content-Type": "application/json",
        # Not sure this is useful
        # "Access-Control-Allow-Origin": "*"
    })

    # Create the output directory
    output_dir = experiment_parameters['dirs']['root']
    output_dir += experiment_parameters['dirs']['output_subdir'].format(
        experiment_name=experiment_parameters['experiment_name'])
    try:
        os.mkdir(output_dir)
    except:
        pass

    output_dir += "experiment{}/".format(experiment_id)
    try:
        os.mkdir(output_dir)
    except FileExistsError as e:
        # Remove existing files if the dir exists
        for f in os.listdir(output_dir):
            os.remove(output_dir+f)
        pass

    print("Writing experiment parameters to", output_dir+PARAMETERS_FN)
    parameters_df = pd.DataFrame()
    parameters_df = parameters_df.append(pd.Series(data=dict(
        initial_sequence_length=experiment_parameters['sequence']['initial_length'],
        total_sequence_length=experiment_parameters['sequence']['total_length'],
        n_devices=experiment_parameters['n_devices'],
        model_type=experiment_parameters['model_type'],
        model_parameters=experiment_parameters['model_generation_parameters'],
        requests_interval=experiment_parameters['requests_interval'],
        **experiment_parameters['program_parameters']
    )), ignore_index=True)
    parameters_df.to_csv(output_dir+PARAMETERS_FN, index=False)

    # Configure host, prepare experiment and start the containers
    containers_name, containers_ip, W, S, past_times = bootstrap_experiment(
        experiment_id, experiment_parameters)
    # print(past_times)

    for device_id, t in zip(S, past_times):
        sequence_df = sequence_df.append(pd.Series({
            "Address": containers_ip[device_id]+":" +
            str(experiment_parameters['net']['gossip_port']),
            "Timestamp": t}), ignore_index=True)

    initial_sequence_length = experiment_parameters['sequence']['initial_length']

    for i, device_id in enumerate(S[initial_sequence_length:]):
        device_ip = containers_ip[device_id]
        device_name = containers_name[device_id]
        url = "http://"+device_ip
        url += ":"+str(experiment_parameters['net']['rest_port']
                       )+experiment_parameters['net']['rest_url']
        json_payload = json.dumps({
            'Session': ''.join([random.choice(string.ascii_lowercase)
                                for _ in range(experiment_parameters['payload_size'])])
        })

        # Rest request to the container
        try:
            r = s.put(url, data=json_payload)
            if(r.status_code != 200):
                print("Error returned: status_code={}, answer={}".format(
                    r.status_code, r.text))
        except:
            print("Error sending: {} {}".format(
                type(sys.exc_info()[0]), sys.exc_info()[1]))
            continue

        request_datetime = datetime.datetime.strptime(
            r.json()['Timestamp'], "%Y-%m-%d %H:%M:%S.%f")
        #request_datetime=datetime.datetime.combine(datetime.datetime.now().date(), request_time.timetz())

        sequence_df = sequence_df.append(pd.Series({
            "Address": containers_ip[device_id]+":" +
            str(experiment_parameters['net']['gossip_port']),
            "Timestamp": request_datetime}), ignore_index=True)

        sleep_time = experiment_parameters['requests_interval']

        print("[Experiment #{} session {}/{}]"
              " Used device {}, now sleeping {:.2f}s".format(
                  experiment_id, initial_sequence_length+i+1,
                  len(S), device_name, sleep_time))

        time.sleep(sleep_time)

    print("[Experiment #{}] Writing sequence to {}...".format(
        experiment_id, output_dir+"sequence.csv"))
    sequence_df.to_csv(output_dir+SEQUENCE_FN, index=False)
    print("[Experiment #{}] Writing model to {}...".format(
        experiment_id, output_dir+"model.npy"))
    np.save(output_dir+MODEL_FN, W)

    # We sleep one last time before removing the containers
    time.sleep(2)

    docker_utils.remove_containers(
        image_name=experiment_parameters['image_name'],
        container_label=experiment_parameters['container']['label'].format(
            experiment_id=experiment_id),
        only_stop=experiment_parameters['container']['only_stop'])

    print("[Experiment #{}] Done!\n\n".format(experiment_id))
