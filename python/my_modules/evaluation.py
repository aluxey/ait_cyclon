#!/usr/bin/env python3

#from plotly import tools
#from plotly.offline import plot
#import copy
#import matplotlib.pyplot as plt
#import plotly.graph_objs as go
from . import handoff
from .constants import *
import ast
import numpy as np
import os
import pandas as pd
import re
import sys


# ---------- Utils ----------

def get_parameter(name, experiment_id, parameters_df):
    return parameters_df[parameters_df['ExperimentId'] == experiment_id][name][0]


def get_experiment_name(experiment_id, parameters_df):
    model_parameters = ""
    if parameters_df[parameters_df['ExperimentId'] == experiment_id]['model_parameters'].values[0] != {}:
        model_parameters = "{}".format(
            parameters_df[parameters_df['ExperimentId'] == experiment_id]['model_parameters'].values[0])

        return "{} {}".format(
            parameters_df[parameters_df['ExperimentId']
                          == experiment_id]['model_type'].values[0],
            model_parameters)
    else:
        return parameters_df[parameters_df['ExperimentId'] == experiment_id]['model_type'].values[0]

# ---------- Splitting and ordering ----------


def splitList(x):
    if pd.isnull(x) or x == '' or x == 'nan':
        return []
    return x.split('|')

# from string addr A.B.C.D:E
# to tuple of ints (A, B, C, D, E)


def addr_to_tuple(addr):
    ret = [None]*5
    ip, ret[4] = addr.split(':')
    ret[0:4] = ip.split('.')
    return tuple(int(item) for item in ret)
# Sorts the adresses list such that X.Y.Z.2 < X.Y.Z.10
# This is the order of the model matrix W


def get_sorted_addresses(addresses):
    return sorted(addresses, key=addr_to_tuple)

# ---------- Fetch experiments output ----------


def fetch_experiment_results(results_dir, experiment_id):
    sessions_df = pd.DataFrame()
    activity_df = pd.DataFrame()
    sequence_df = pd.DataFrame()
    parameters_df = pd.DataFrame()
    network_df = pd.DataFrame()
    W = None

    print("[fetch_experiment_results #{}] Fetching data from {}...".format(
        experiment_id, results_dir))

    for fn in os.listdir(results_dir):
        filepath = results_dir+fn
        if re.search(ACTIVITY_FILE_EXPRESSION, fn):
            try:
                #df=pd.read_csv(filepath, parse_dates=['Timestamp'], index_col=['Timestamp'])
                df = pd.read_csv(filepath, parse_dates=['Timestamp'])
            except:
                print("Failed loading {}: {}".format(fn, sys.exc_info()))
                pass
            else:
                if(activity_df.empty):
                    activity_df = df
                else:
                    activity_df = activity_df.append(df,)
                print("Loaded activity dataframe {}.".format(fn))

        elif re.search(SESSION_FILE_EXPRESSION, fn):
            try:
                #df=pd.read_csv(filepath, parse_dates=['Timestamp', 'SessionTimestamp'], index_col=['Timestamp'])
                df = pd.read_csv(filepath, parse_dates=[
                                 'Timestamp', 'SessionTimestamp'])
            except:
                print("Failed loading {}: {}".format(fn, sys.exc_info()))
                pass
            else:
                if(sessions_df.empty):
                    sessions_df = df
                else:
                    sessions_df = sessions_df.append(df,)
                print("Loaded sessions dataframe {}.".format(fn))
        elif re.search(SEQUENCE_FN, fn):
            try:
                #sequence_df=pd.read_csv(filepath, parse_dates=['Timestamp'], index_col=['Timestamp'])
                sequence_df = pd.read_csv(filepath, parse_dates=['Timestamp'])
                print("Loaded sequence dataframe {}.".format(fn))
            except:
                print("Failed loading {}: {}".format(fn, sys.exc_info()))
                pass
        elif re.search(MODEL_FN, fn):
            try:
                W = np.load(filepath)
                print("Loaded model matrix {}.".format(fn))
            except:
                print("Failed loading {}: {}".format(fn, sys.exc_info()))
                pass
        elif re.search(PARAMETERS_FN, fn):
            try:
                parameters_df = parameters_df.append(pd.read_csv(filepath))
                print("Loaded parameters dataframe {}.".format(fn))
            except:
                print("Failed loading {}: {}".format(fn, sys.exc_info()))
                pass
        elif re.search(NETWORK_FILE_EXPRESSION, fn):
            try:
                network_df = network_df.append(pd.read_csv(filepath))
                print("Loaded network dataframe {}.".format(fn))
            except:
                print("Failed loading {}: {}".format(fn, sys.exc_info()))
                pass

    print("Arranging the data...")
    #activity_df = activity_df.drop(['BeforeSet', 'AfterSet'], axis=1)
    activity_df['SequenceTimestamps'] = activity_df['SequenceTimestamps'].apply(
        lambda row: [pd.to_datetime(x) for x in splitList(row)])
    activity_df['SessionTimestamp'] = activity_df.apply(
        lambda row: row['SequenceTimestamps'][-1], axis=1)
    activity_df['SequenceLength'] = activity_df.apply(
        lambda row: len(row['SequenceTimestamps']), axis=1)
    activity_df.fillna("", inplace=True)
    activity_df.sort_values("Timestamp", inplace=True)
    activity_df['ExperimentId'] = experiment_id

    sessions_df.fillna("", inplace=True)
    sessions_df.sort_values("Timestamp", inplace=True)
    sessions_df['ExperimentId'] = experiment_id

    sequence_df.fillna("", inplace=True)
    sequence_df.sort_values("Timestamp", inplace=True)
    sequence_df['ExperimentId'] = experiment_id

    # parameters_df=parameters_df.astype(np.int)
    parameters_df['model_parameters'] = parameters_df[
        'model_parameters'].map(lambda d: ast.literal_eval(d))
    parameters_df['ExperimentId'] = experiment_id

    network_df.fillna("", inplace=True)
    network_df = network_df[(network_df['nRec'] != 0)
                            | (network_df['nSent'] != 0)]
    network_df.sort_values("Timestamp", inplace=True)
    network_df['ExperimentId'] = experiment_id

    print("[fetch_experiment_results #{}] All done!\n".format(experiment_id))

    return activity_df, network_df, parameters_df, sequence_df, sessions_df, W

# ---------- Sequence and rounds ----------


def get_rounds_time_after_init(sequence_df, parameters_df, experiment_id):
    start_time = sequence_df['Timestamp'].iloc[
        int(get_parameter('initial_sequence_length', experiment_id, parameters_df))]
    return sequence_df[(sequence_df['ExperimentId'] == experiment_id) &
                       (sequence_df['Timestamp'] >= start_time)]['Timestamp'].values


def get_sequence_after_init(sequence_df, parameters_df, experiment_id):
    start_time = sequence_df['Timestamp'].iloc[
        int(get_parameter('initial_sequence_length', experiment_id, parameters_df))]

    return sequence_df[(sequence_df['ExperimentId'] == experiment_id) &
                       (sequence_df['Timestamp'] >= start_time)]

# def get_round_address(sequence_df, experiment_id, round_time):
#     return sequence_df['Address'][(sequence_df['ExperimentId'] == experiment_id) &
#                        (sequence_df['Timestamp'] == round_time)].values[0]


def get_local_sequence(sequence_df, activity_df, experiment_id, device, next_sts):
    # Get the timestamps known by device before next_sts
    S_d_timestamps = activity_df[
        (activity_df['ExperimentId'] == experiment_id) &
        (activity_df['Address'] == device) &
        (activity_df['Timestamp'] < next_sts)
    ]['SequenceTimestamps'].iloc[-1]

    # print(activity_df[
    #         (activity_df['ExperimentId'] == experiment_id) &
    #         (activity_df['Address'] == device) &
    #         (activity_df['Timestamp'] < next_sts)
    #     ])
    # last_timestamp=activity_df[
    #         (activity_df['ExperimentId'] == experiment_id) &
    #         (activity_df['Address'] == device) &
    #         (activity_df['Timestamp'] < next_sts)
    #     ].iloc[-1]['Timestamp']
    # print("last timestamp:", last_timestamp, type(last_timestamp))
    # print("next_sts:", next_sts, type(next_sts))
    # print("last_timestamp < next_sts?", last_timestamp < next_sts)
    # print(S_d_timestamps)

    # From this, get current_device's local sequence S_d
    return sequence_df[
        (sequence_df['ExperimentId'] == experiment_id) &
        (sequence_df['Timestamp'].isin(S_d_timestamps))
    ][['Address', 'Timestamp']]


# ---------- Compute handoff cost results ----------

# gamma is the factor applied to the quantity of the session chunk shared
def compute_handoff_cost(activity_df, parameters_df, sequence_df,
                         n_experiments, gammas=[1], variance_factor=0.,
                         min_proba=0, apply_smoothing=False):
    # output dataframe
    cost_df = pd.DataFrame()

    # for each experiment
    for experiment_id in range(n_experiments):
        print("[compute_handoff_cost #{}: {} ] Let's get started...".format(
            experiment_id, get_experiment_name(experiment_id, parameters_df)))

        # Whole sequence
        S = sequence_df[sequence_df['ExperimentId'] == experiment_id]
        # After init
        #S_after_init=get_sequence_after_init(S, parameters_df, experiment_id)

        # For each round after init (excluding the last round)
        for i in range(
                int(get_parameter('initial_sequence_length',
                                  experiment_id, parameters_df)),
                len(S)-1):
            # current_round and next_round are pandas.core.series.Series
            # Access Address: current_round['Address']
            current_round = S.iloc[i]
            next_round = S.iloc[i+1]

            # # Next timestamp
            # next_sts=rounds_timestamp[i+1]

            # # Current and next devices
            # current_device=get_round_address(S, experiment_id, sts)
            # next_device=get_round_address(S, experiment_id, next_sts)

            # # Get the timestamps known by current_device before next_sts
            # S_d_timestamps=activity_df[
            #         (activity_df['ExperimentId'] == experiment_id) &
            #         (activity_df['Address'] == current_device) &
            #         (activity_df['Timestamp'] < next_sts)
            #     ]['SequenceTimestamps'].iloc[-1]
            # # From this, get current_device's local sequence S_d
            # S_d=S[S['Timestamp'].isin(S_d_timestamps)][['Address', 'Timestamp']]

            S_d = get_local_sequence(S, activity_df, experiment_id,
                                     current_round['Address'], next_round['Timestamp'])

            # Compute the probability of switching to other devices from S_d
            # We compute a smoothed probability to avoid zero values
            P = handoff.get_transition_probability(S_d, current_round['Address'],
                                                   apply_smoothing=apply_smoothing)

            for gamma in gammas:

                # current round's output row
                result = pd.Series({
                    'ExperimentId': experiment_id,
                    'Device': current_round['Address'],
                    'Timestamp': current_round['Timestamp'],
                    'NextDevice': next_round['Address'],
                    'NextTimestamp': next_round['Timestamp'],
                    'PNextDevice': P[next_round['Address']]
                    if next_round['Address'] in P else 0,
                })

                # if experiment_id == 1:
                #    handoff.DEBUG=True
                # print("\n\nIteration #{} (len(S_d)={})".format(i, len(S_d)))

                # else:
                #    handoff.DEBUG=False

                result = handoff.send_session_by_case_draft(
                    P, current_round, next_round,
                    experiment_id, activity_df, sequence_df, result, gamma)

                cost_df = cost_df.append(result, ignore_index=True)

    print("[compute_handoff_cost] All done!")
    return cost_df
