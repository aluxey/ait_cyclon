package network

import (
	"errors"
	"fmt"
	"net"
	"os"
)

func GetMyAddress(netAddr string) (string, error) {
	if MyIP, err := GetMyIP(netAddr); err != nil {
		return "", err
	} else {
		return MyIP.String() + ":" + UDP_PORT, nil
	}
}

// This function attempts to retrieve the network IP we will use
// If no netAddr is provided, it calls GetOverlayIP, that will look for an interface w/ MTU==1450 (a Docker network w/ "overlay" driver)
// netAddr must contain a network address in CIDR notation: "x.y.z.t/m"
func GetMyIP(netAddr string) (net.IP, error) {
	if netAddr == "" {
		return GetOverlayIP()
	}

	netIP, _, err := net.ParseCIDR(netAddr)
	if err != nil {
		return nil, err
	}
	ifaceAddrs, err := net.InterfaceAddrs()
	if err != nil {
		return nil, err
	}

	for _, ifaceAddr := range ifaceAddrs {
		ifaceIP, ifaceNetIP, err := net.ParseCIDR(ifaceAddr.String())
		//fmt.Fprintf(os.Stderr, "Iface#%v: %v\n", i, ifaceAddr)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error parsing iface's CIDR Address: %v\n", err)
			continue
		}

		if ifaceNetIP.Contains(netIP) {
			fmt.Printf("[GetMyAddr] We found an IP corresponding to network '%v': %v\n", netAddr, ifaceIP)
			return ifaceIP, nil
		}
	}

	return nil, fmt.Errorf("[GetMyAddr] Error: no interface connected to the network '%v'.\n", netAddr)
}

// This function looks for an interface with MTU=1450:
// It is the overlay address with our overlay setup.
// If it finds it, it looks for the external IPv4 to retrieve.
func GetOverlayIP() (net.IP, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	var eth net.Interface
	found := false
	for _, iface := range ifaces {
		//fmt.Printf("Found interface \"%v\"\n", iface.Name)
		if iface.MTU == 1450 {
			eth = iface
			found = true
			fmt.Printf("[GetOverlayIP] Overlay network found on iface %v\n", iface.Name)
			break
		}
	}
	if !found {
		return nil, errors.New("[GetOverlayIP] Could not find the overlay network (w/ MTU of 1450).")
	}

	addrs, err := eth.Addrs()
	if err != nil {
		return nil, err
	}

	//fmt.Printf("[GetOverlayIP] Interface %v contains following IPs: %v\n", eth.Name, eth.Addrs)

	for _, addr := range addrs {
		var ip net.IP
		switch v := addr.(type) {
		case *net.IPNet:
			ip = v.IP
		case *net.IPAddr:
			ip = v.IP
		}
		// I think this is useless
		if ip == nil || ip.IsLoopback() {
			continue
		}
		// Does this just obliges us to use IPv4 or is it useful?
		ip = ip.To4()
		if ip != nil {
			fmt.Printf("[GetOverlayIP] Our IP on the overlay is: %v\n", ip)
			return ip, nil
		}
	}
	return nil, errors.New("[GetOverlayIP] We could not find a valid IPv4 address on the overlay net.")
}
