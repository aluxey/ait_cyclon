package network

type MessageType string

type Message struct {
	Type    MessageType
	Payload []byte
}
