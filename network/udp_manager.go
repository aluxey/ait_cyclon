package network

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"net"
	"os"
	"sync"
)

const (
	UDP_PORT         = "10337"
	DEFAULT_BUF_SIZE = 1400
	DEBUG_UDP        = false
)

type UDPManager struct {
	// Observer pattern to map datagram type to communication channel
	// Used in dispatch and RegisterObserver
	// We wanted this:
	//observers map[MessageType]chan<- Message
	// But Go's map ain't safe for concurrent use
	// So: (cf https://blog.golang.org/go-maps-in-action#TOC_6.)
	observers struct {
		sync.RWMutex
		m map[MessageType]chan<- Message
	}

	pc net.PacketConn

	// stopCh channel: closing it escapes infinite loops (daemons)
	stopCh  chan struct{}
	running bool

	myAddr string
	// Most logical would be to set the bufSize to the MTU...
	bufSize int

	// For monitoring
	mutex                            sync.Mutex
	bytesRec, bytesSent, nRec, nSent int
}

func CreateUDPManager(MyAddr string) (*UDPManager, error) {
	nm := new(UDPManager)

	// If we gave only ":port" to ListenPacket, pc (PacketConn) would listen on all available interfaces
	// Which is quite cool
	var err error
	nm.pc, err = net.ListenPacket("udp", MyAddr)
	if err != nil {
		return nil, err
	}

	nm.stopCh = make(chan struct{})
	nm.running = true

	nm.myAddr = MyAddr

	nm.observers.m = make(map[MessageType]chan<- Message)

	nm.bufSize = DEFAULT_BUF_SIZE

	go nm.receiveDaemon()

	return nm, nil
}

func (nm *UDPManager) Send(dtg Message, recipientAddr string) error {
	select {
	case <-nm.stopCh:
		return fmt.Errorf("[network.UDPManager.Send] The network manager is closed.")
	default:
		// Create a buffer to serialize the datagram into
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		if err := enc.Encode(&dtg); err != nil {
			return fmt.Errorf("[network.UDPManager.Send] Error while encoding datagram: %v\n\n", err)
		}

		//nBytes := buf.Len()

		addr, err := net.ResolveUDPAddr("udp", recipientAddr)
		if err != nil {
			return fmt.Errorf("[network.UDPManager.Send] Error resolving UDP address: %v\n", err)
		}
		nBytes := 0
		if nBytes, err = nm.pc.WriteTo(buf.Bytes(), addr); err != nil {
			return fmt.Errorf("[network.UDPManager.Send] Error while sending datagram of size %d: %v\n",
				buf.Len(), err)
		} else {
			// Give information to monitoring
			nm.mutex.Lock()
			nm.bytesSent += nBytes
			nm.nSent++
			nm.mutex.Unlock()
		}

		debugUDP("[network.UDPManager.Send] Sent datagram (%vB) of type %q to %v.\n\n",
			nBytes, dtg.Type, recipientAddr)

		// Send #bytes sent to Monitoring via channel
		// sendCh <- nBytes

		return nil
	}
}

func (nm *UDPManager) RegisterObserver(dtgType MessageType, ch chan<- Message) error {
	nm.observers.Lock()
	defer nm.observers.Unlock()

	if nm.observers.m[dtgType] == nil {
		debugUDP("[network.UDPManager.RegisterObserver] Observer registered for MessageType %q.\n", dtgType)
		nm.observers.m[dtgType] = ch
		return nil
	} else {
		return fmt.Errorf("[network.UDPManager.RegisterObserver] An observer already exists for MessageType %q.\n", dtgType)
	}
}

func (nm *UDPManager) receiveDaemon() {
	// Received datagram buffer
	buf := make([]byte, nm.bufSize)

	for {
		select {
		case <-nm.stopCh:
			break
		default:
			if nBytes, addr, err := nm.pc.ReadFrom(buf); err != nil {
				fmt.Fprintf(os.Stderr, "[network.UDPManager.receiveDaemon] Error while receiving datagram: %v\n", err)
				continue
			} else {
				go nm.dispatch(buf, addr.String(), nBytes)
				// Give information to monitoring
				nm.mutex.Lock()
				nm.bytesRec += nBytes
				nm.nRec++
				nm.mutex.Unlock()
			}
		}
	}

}

func (nm *UDPManager) dispatch(buf []byte, addr string, nBytes int) {
	// Decode it
	var dtg Message
	decoderBuf := bytes.NewBuffer(buf)
	dec := gob.NewDecoder(decoderBuf)
	if err := dec.Decode(&dtg); err != nil {
		fmt.Fprintf(os.Stderr, "[network.UDPManager.dispatch] Error while decoding datagram of %vB: %v\n", nBytes, err)
		// Increase buf size in case of buffer overflow (though default size of 1024 can handle >20 Descriptors)
		if err.Error() == "unexpected EOF" {
			// This modifying nm.bufSize is NOT thread-safe!
			// It will also overflow the MTU: it's all wrong.
			// (But we said the buffer would be hard to overflow. We'll see when it happens.)
			nm.bufSize = int(float32(nm.bufSize) * 1.5)
			debugUDP("[network.UDPManager.dispatch] Increasing buffer size to %d because of error: %v\n",
				nm.bufSize, err)
		}
		return
	}

	debugUDP("[network.UDPManager.dispatch] Received datagram (%vB) of type %q from %v.\n\n",
		nBytes, dtg.Type, addr)

	nm.observers.RLock()
	if nm.observers.m[dtg.Type] != nil {
		nm.observers.m[dtg.Type] <- dtg
	} else {
		fmt.Fprintf(os.Stderr, "[network.UDPManager.dispatch] No observer subscribed to %q.\n", dtg.Type)
	}
	nm.observers.RUnlock()
}

// I'm just writing this for the record, but we **should** Close the PacketConn at some point.
// If we weren't in a container that's going to be killed anyway...
func (nm *UDPManager) Stop() {
	close(nm.stopCh)
	nm.pc.Close()
	nm.running = false
}

func debugUDP(format string, a ...interface{}) {
	if DEBUG_UDP {
		fmt.Printf(format, a...)
	}
}
