package network

import (
	//"bytes"
	"encoding/gob"
	"fmt"
	//"io"
	"net"
	"os"
	"sync"
)

const (
	TCP_BUF_SIZE    = 10000
	TCP_TIMEOUT     = 2 // seconds
	TCP_LISTEN_PORT = 10337
	DEBUG_TCP       = false
)

type TCPManager struct {
	observers struct {
		// This map needs a mutex to prevent concurrent use
		sync.RWMutex
		m map[MessageType]chan<- Message
	}

	// The keys of connections are the IPs (not IP:PORT) of the remotes
	connections struct {
		sync.RWMutex
		// For each remote, we keep a connection opened, and the encoder
		// The decoder is tied to the connection created by AcceptTCP, no need to keep it
		mConn map[string]*net.TCPConn
		mEnc  map[string]*gob.Encoder
	}

	ln *net.TCPListener

	myAddr  string
	bufSize int

	// handle to stop the daemon
	stopCh chan struct{}

	// For monitoring
	monitor                          bool
	mutex                            sync.Mutex
	bytesRec, bytesSent, nRec, nSent int
}

func CreateTCPManager(myAddr string, monitor bool) (*TCPManager, error) {
	nm := new(TCPManager)

	var err error
	// Error, error, I don't believe in your existence
	myTcpAddr, _ := net.ResolveTCPAddr("tcp", myAddr)
	// Creation of the listener on TCP_LISTEN_PORT
	myTcpAddr.Port = TCP_LISTEN_PORT
	nm.ln, err = net.ListenTCP("tcp", myTcpAddr)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[CreateTCPManager] Failed initialising TCP Listener at %q.\n", myAddr)
		return nil, err
	}

	nm.myAddr = myAddr
	nm.stopCh = make(chan struct{})
	nm.bufSize = DEFAULT_BUF_SIZE

	nm.observers.m = make(map[MessageType]chan<- Message)

	nm.connections.mConn = make(map[string]*net.TCPConn)
	nm.connections.mEnc = make(map[string]*gob.Encoder)
	//nm.connections.mDec = make(map[string]*gob.Decoder)

	nm.monitor = monitor

	go nm.receiveDaemon()

	return nm, nil
}

func (nm *TCPManager) RegisterObserver(messType MessageType, ch chan<- Message) error {
	nm.observers.Lock()
	defer nm.observers.Unlock()

	if nm.observers.m[messType] == nil {
		debugTCP("[TCPManager.RegisterObserver] Observer registered for MessageType %q.\n", messType)
		nm.observers.m[messType] = ch
		return nil
	} else {
		return fmt.Errorf("[TCPManager.RegisterObserver] An observer already exists for MessageType %v.\n", messType)
	}
}

func (nm *TCPManager) Stop() {
	close(nm.stopCh)
	nm.connections.RLock()
	for _, conn := range nm.connections.mConn {
		conn.Close()
	}
	nm.ln.Close()
}

func (nm *TCPManager) Send(mess Message, recipientAddr string) error {
	select {
	case <-nm.stopCh:
		return fmt.Errorf("[TCPManager.Send] The network manager is closed.")
	default:

		// secondAttempt will hold true if we failed sending once. We will then retry
		secondAttempt := false
		for {
			// Getting the connection and encoder for recipientAddr (will be created if inexistent)
			conn, enc, err := nm.getConnection(recipientAddr)
			if err != nil {
				return fmt.Errorf("[TCPManager.Send] Could not get TCP connection with %q: %v\n\n",
					recipientAddr, err)
			}
			recipientAddr = conn.RemoteAddr().String()

			// Trying to encode the message into the connection
			if err := enc.Encode(&mess); err != nil {
				// If it fails once
				if !secondAttempt {
					fmt.Fprintf(os.Stderr, "[TCPManager.Send] Failed encoding %v for %q: %v\n", mess.Type, recipientAddr, err)
					fmt.Fprintf(os.Stderr, "[TCPManager.Send] ... we will retry once with a new connection.\n")

					// We delete the connection and try again
					nm.deleteConnection(recipientAddr)
					secondAttempt = true

					continue
				} else {
					// If it failed a second time, we delete the connection and return an error
					nm.deleteConnection(recipientAddr)
					return fmt.Errorf("[TCPManager.Send] Failed encoding %v for %q a second time: %v\n"+
						"[TCPManager.Send] ... we close the connection and abort.", mess.Type, recipientAddr, err)
				}
			} else {
				if nm.monitor {
					nm.mutex.Lock()
					nm.nSent += 1
					nm.bytesSent += len(mess.Payload)
					nm.mutex.Unlock()
				}
				// Return no error if the transmission succeeded
				return nil
			}
		}
	}
}

func (nm *TCPManager) receiveDaemon() {
	for {
		select {
		case <-nm.stopCh:
			break
		default:
			debugTCP("[TCPManager.receiveDaemon] Listening for incoming connections...\n")

			// Listener listens on TCP_LISTEN_PORT
			if conn, err := nm.ln.AcceptTCP(); err != nil {
				fmt.Fprintf(os.Stderr,
					"[TCPManager.receiveDaemon] Failed receiving connection: %v\n", err)
			} else {
				debugTCP("[TCPManager.receiveDaemon] Accepted a new connection: \"%v->%v\".\n\n",
					conn.LocalAddr(), conn.RemoteAddr())
				nm.setConnection(conn)
				go nm.listenConnection(conn)
			}
		}
	}
}

func (nm *TCPManager) listenConnection(conn *net.TCPConn) {
	// The decoder exists as long as the connection functions. It will be deleted when listenConnection fails
	dec := gob.NewDecoder(conn)

	senderAddr := conn.RemoteAddr().String()

	// Loops for each new message as long as the connection does not bug
	for {
		select {
		case <-nm.stopCh:
			return
		default:

			var mess Message
			if err := dec.Decode(&mess); err != nil {
				fmt.Fprintf(os.Stderr, "[TCPManager.listenConnection] Error while decoding message from %q : %v\n",
					senderAddr, err)
				fmt.Fprintf(os.Stderr, "[TCPManager.listenConnection] ... we close the connection and exit.\n")

				// We delete the connection and return
				nm.deleteConnection(senderAddr)
				return
			}

			if nm.monitor {
				nm.mutex.Lock()
				nm.nRec += 1
				nm.bytesRec += len(mess.Payload)
				nm.mutex.Unlock()
			}

			debugTCP("[TCPManager.listenConnection] Received %v from %q.\n\n",
				mess.Type, senderAddr)

			nm.observers.RLock()
			if nm.observers.m[mess.Type] != nil {
				nm.observers.m[mess.Type] <- mess
			} else {
				fmt.Fprintf(os.Stderr, "[TCPManager.listenConnection] No observer subscribed to %q.\n", mess.Type)
			}
			nm.observers.RUnlock()
		}
	}
}

func (nm *TCPManager) deleteConnection(recipientAddr string) {
	nm.connections.Lock()
	defer nm.connections.Unlock()

	recipientTcpAddr, _ := net.ResolveTCPAddr("tcp", recipientAddr)
	recipientIP := recipientTcpAddr.IP.String()

	delete(nm.connections.mConn, recipientIP)
	delete(nm.connections.mEnc, recipientIP)
	//delete(nm.connections.mDec, recipientIP)
}

func (nm *TCPManager) getConnection(recipientAddr string) (*net.TCPConn, *gob.Encoder /*, *gob.Decoder */, error) {
	nm.connections.Lock()
	defer nm.connections.Unlock()

	if recipientAddr == nm.myAddr {
		return nil, nil, fmt.Errorf("You tried to connect to yourself, dumbass.")
	}

	recipientTcpAddr, err := net.ResolveTCPAddr("tcp", recipientAddr)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[TCPManager.getConnection] Error resolving recipient TCP address: %v\n",
			err)
		return nil, nil, err
	}

	recipientIP := recipientTcpAddr.IP.String()

	if conn, ok := nm.connections.mConn[recipientIP]; !ok {
		var err error

		// This is the ONLY PLACE where we CREATE a connection using DialTCP
		// setConnection only inserts a connection created remotely inside the connections map

		// We dial the remote on its TCP_LISTEN_PORT
		recipientTcpAddr.Port = TCP_LISTEN_PORT

		// Very important to put nil here: the system will find an available port for us
		// If we put always the same port, the bind would only work once
		if conn, err = net.DialTCP("tcp", nil, recipientTcpAddr); err != nil {
			return nil, nil, err
		} else {
			debugTCP("[TCPManager.getConnection] Created a new connection with %q: \"%v->%v\".\n", recipientIP, conn.LocalAddr(), conn.RemoteAddr())
			conn.SetKeepAlive(true)

			nm.connections.mConn[recipientIP] = conn
			nm.connections.mEnc[recipientIP] = gob.NewEncoder(conn)
			//nm.connections.mDec[recipientIP] = gob.NewDecoder(conn)
			go nm.listenConnection(conn)

			//return conn, nm.connections.mEnc[recipientIP], nm.connections.mDec[recipientIP], nil
			return conn, nm.connections.mEnc[recipientIP], nil
		}
	} else {
		//return conn, nm.connections.mEnc[recipientIP], nm.connections.mDec[recipientIP], nil
		return conn, nm.connections.mEnc[recipientIP], nil
	}

}

func (nm *TCPManager) setConnection(conn *net.TCPConn) {
	nm.connections.Lock()
	defer nm.connections.Unlock()

	recipientTcpAddr, _ := net.ResolveTCPAddr("tcp", conn.RemoteAddr().String())
	recipientIP := recipientTcpAddr.IP.String()

	if conn2, ok := nm.connections.mConn[recipientIP]; ok {
		fmt.Fprintf(os.Stderr, "[TCPManager.setConnection] A connection already existed with %q:%v->%v\n\t... Closing it.\n\n",
			recipientIP, conn2.LocalAddr(), conn2.RemoteAddr())
		conn2.Close()
		delete(nm.connections.mConn, recipientIP)
		delete(nm.connections.mEnc, recipientIP)
		//delete(nm.connections.mDec, recipientIP)
	}

	conn.SetKeepAlive(true)
	nm.connections.mConn[recipientIP] = conn
	nm.connections.mEnc[recipientIP] = gob.NewEncoder(conn)
	//nm.connections.mDec[recipientIP] = gob.NewDecoder(conn)
}

func debugTCP(format string, a ...interface{}) {
	if DEBUG_TCP {
		fmt.Printf(format, a...)
	}
}
