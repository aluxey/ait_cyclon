package network

// Network manager interface
type Manager interface {
	Send(mess Message, recipientAddr string) error
	RegisterObserver(messType MessageType, ch chan<- Message) error
}
