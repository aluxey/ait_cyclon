package network

import (
	"fmt"
	"strconv"
	"time"
)

// Implement the methods to satisfy the monitoring/monitorable.go "Monitorable" interface

func (nm *UDPManager) GetFilename() string {
	now := time.Now()
	return fmt.Sprintf("udpnetwork-%v_%02d-%02d-%02d_%02dh%02d.csv",
		nm.myAddr,
		now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
}

func (nm *UDPManager) GetHeader() []string {
	return []string{"Address", "Timestamp", "bytesRec", "nRec", "bytesSent", "nSent"}
}

func (nm *UDPManager) GetLine() ([]string, bool, error) {
	nm.mutex.Lock()
	defer nm.mutex.Unlock()

	if !nm.IsRunning() {
		return []string{}, false, fmt.Errorf("Network UDPManager is not running.")
	}

	ret := []string{nm.myAddr, time.Now().Format(time.RFC3339),
		strconv.Itoa(nm.bytesRec), strconv.Itoa(nm.nRec),
		strconv.Itoa(nm.bytesSent), strconv.Itoa(nm.nSent)}

	// Reinit the counters
	nm.bytesRec, nm.nRec, nm.bytesSent, nm.nSent = 0, 0, 0, 0

	return ret, false, nil
}

func (nm *UDPManager) IsRunning() bool { return nm.running }
