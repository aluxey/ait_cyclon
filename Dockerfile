FROM library/alpine

# EXPOSE 10337/udp

ADD ait_cyclon /

ENTRYPOINT [ "/ait_cyclon" ]

# default parameter to ENTRYPOINT
CMD [ "--help" ]
