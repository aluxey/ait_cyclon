/*
Author: A. Luxey
Date: 01/2017

This package contains a singleton that writes logs in ShiViz format.
Find the ShiViz project here: http://bestchai.bitbucket.org/shiviz/
The log format we will use is the following (cf ShiViz's documentation):
"<timestamp> <event message>\n
<host> <address> <vector clock>\n"
*/

package shiviz

import (
	"fmt"
	"os"
	"sync"
	"time"
)

type ShiViz struct {
	fn string // Output filename
	f  *os.File

	clock   uint32
	profile string
	address string

	mutex sync.Mutex
}

// This pointer holds the singleton, don't use it as is.
// Call shiviz.Get() to retrieve it, and shiviz.Init() to initialize it
var shiViz *ShiViz

func IsShiViz() bool {
	return shiViz != nil
}

func Init(monitorDir, profile, addr string) error {
	if shiViz != nil {
		return fmt.Errorf("[shiviz.Init] ShiViz was already initialized with fn = %v",
			shiViz.fn)
	}

	shiViz = new(ShiViz)

	shiViz.fn = fmt.Sprintf("%v/%v-%v.shiviz", monitorDir, profile, addr)

	var err error
	shiViz.f, err = os.Create(shiViz.fn)
	if err != nil {
		return fmt.Errorf("[shiviz.Init] Couldn't create file %v: %v", shiViz.fn, err)
	}

	// We write a first line, separating different logs when we will concat them
	// line := fmt.Sprintf("=== %v ===\n", profile)
	// if _, err = shiViz.f.WriteString(line); err != nil {
	// 	return err
	// }

	shiViz.clock = 1
	shiViz.profile = profile
	shiViz.address = addr

	return nil
}

func WriteLocal(msg string, data string) error {
	if shiViz == nil {
		return fmt.Errorf("[shiviz.Write] ShiViz was not initialized.")
	}

	shiViz.mutex.Lock()

	line := fmt.Sprintf("[%s] %v\n%v %v {\"%v\": %d}\n%v;;\n", time.Now(), msg,
		shiViz.profile, shiViz.address, shiViz.profile, shiViz.clock, data)

	if _, err := shiViz.f.WriteString(line); err != nil {
		return err
	}
	// shiViz.f.Flush()
	// if err = shiViz.f.Error(); err != nil {
	// 	return err
	// }

	shiViz.clock += 1

	shiViz.mutex.Unlock()

	return nil
}

func WriteExchange(msg string, data string, otherHost string, otherClock uint32) error {
	if shiViz == nil {
		return fmt.Errorf("[shiviz.Write] ShiViz was not initialized.")
	}

	shiViz.mutex.Lock()

	line := fmt.Sprintf("[%s] %v\n%v %v {\"%v\": %d, \"%v\": %d}\n%v;;\n", time.Now(), msg,
		shiViz.profile, shiViz.address, shiViz.profile, shiViz.clock,
		otherHost, otherClock, data)

	if _, err := shiViz.f.WriteString(line); err != nil {
		return err
	}
	// shiViz.f.Flush()
	// if err = shiViz.f.Error(); err != nil {
	// 	return err
	// }

	shiViz.clock += 1

	shiViz.mutex.Unlock()

	return nil
}

func GetClock() (uint32, error) {
	if shiViz == nil {
		return 0, fmt.Errorf("[shiviz.Write] ShiViz was not initialized.")
	}
	shiViz.mutex.Lock()
	defer shiViz.mutex.Unlock()
	if shiViz == nil {
		return 0, fmt.Errorf("[shiviz.Write] ShiViz was not initialized.")
	}

	//defer func() { shiViz.clock += 1 }()
	return shiViz.clock, nil
}

func Close() {
	if shiViz != nil {
		shiViz.f.Close()
	}
}
