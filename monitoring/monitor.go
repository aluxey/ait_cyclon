package monitoring

import (
	"encoding/csv"
	"fmt"
	"os"
	"time"
)

const (
	MONITORING_PERIOD = 1 * time.Second
)

var MonitoringDir string = ""

// type that should be embedded by types to he able to create CSV reports
type Monitor struct {
	stopCh chan struct{}

	csvWriter *csv.Writer
	file      *os.File

	monitorable Monitorable

	running bool
}

func (m *Monitor) Init(monitorable Monitorable) error {
	m.monitorable = monitorable

	fn := fmt.Sprintf("%s/%s",
		MonitoringDir, m.monitorable.GetFilename())

	// File creation
	if m.file == nil {
		var err error
		if m.file, err = os.Create(fn); err != nil {
			return fmt.Errorf("[monitoring.Monitor] Couldn't create file %v, exiting program: %v\n", fn, err)
		}
	}

	m.csvWriter = csv.NewWriter(m.file)
	if err := m.csvWriter.Write(m.monitorable.GetHeader()); err != nil {
		return fmt.Errorf("[monitoring.Monitor] Failed to write headers, exiting program: %v\n", err)
	}

	cwd, _ := os.Getwd()
	fmt.Printf("[monitoring.Monitor] We are writing monitoring data to %v/%v .\n", cwd, fn)

	return nil
}

func (m *Monitor) Loop() {
	m.running = true
	m.stopCh = make(chan struct{})

	ticker := time.NewTicker(MONITORING_PERIOD)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			go m.writeLine()
		case <-m.stopCh:
			break
		}
	}
}

func (m *Monitor) Stop() {
	close(m.stopCh)
	m.csvWriter.Flush()
	//m.file.Close() // Yes no, maybe? I don't know...
	m.running = false
}

func (m *Monitor) IsRunning() bool { return m.running }

func (m *Monitor) writeLine() {
	if m.monitorable.IsRunning() {
		if line, hasMore, err := m.monitorable.GetLine(); err != nil {
			fmt.Fprintf(os.Stderr,
				"[monitoring.writeLine] Monitorable could not give a line: %v\n", err)
		} else if len(line) == 0 {
			return
		} else {
			if err := m.csvWriter.Write(line); err != nil {
				fmt.Fprintf(os.Stderr,
					"[monitoring.writeLine] Failed to write current line: %v\n", err)
			} else {
				m.csvWriter.Flush()
				// If the monitorable has more updates pending, call writeLine again
				if hasMore {
					go m.writeLine()
				}
			}
		}
	}
}
