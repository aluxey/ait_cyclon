package monitoring

// Interface that need to be implemented by types that want to report activity CSVs
type Monitorable interface {
	GetFilename() string

	GetHeader() []string
	// returns the line, a "hasMore" boolean to asks for another call to writeLine, and the potential error
	GetLine() ([]string, bool, error)

	IsRunning() bool
}
