package session

import (
	"fmt"
	"time"
)

func (sm *SessionManager) GetFilename() string {
	now := time.Now()
	return fmt.Sprintf("session-%v_%02d-%02d-%02d_%02dh%02d.csv",
		sm.myself.ID(),
		now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
}

func (sm *SessionManager) GetHeader() []string {
	return []string{"ID", "Address", "Timestamp", "Sender", "SessionTimestamp", "DeviceUsed"}
}

func (sm *SessionManager) GetLine() ([]string, bool, error) {
	sm.monitorMutex.Lock()
	defer sm.monitorMutex.Unlock()

	if len(sm.monitorLines) == 0 {
		// Monitor won't write empty lines
		return []string{}, false, nil
	}

	// We pop oldest element first
	ret := sm.monitorLines[0]
	// And remove it from monitorLines
	sm.monitorLines = sm.monitorLines[1:]

	// returns the line, and set "hasMore" boolean if monitorLines is not empty
	return ret, (len(sm.monitorLines) > 0), nil
}

func (sm *SessionManager) IsRunning() bool { return true }
