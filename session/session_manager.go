package session

import (
	"ait_cyclon/gossip"
	"ait_cyclon/gossip/activity_detection"
	//"ait_cyclon/gossip/cyclon"
	"ait_cyclon/network"
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"os"
	"sync"
	"time"
)

const (
	HTTP_PORT         = "10338"
	SESSION_REST_PATH = "/session"

	// Sender string to end to sm.UpdateSession when update is local
	LOCAL_SENDER = "local"

	DEBUG_SM = true
)

type SessionManager struct {
	myself      gossip.Descriptor
	userSession UserSession

	mutex sync.RWMutex

	am activity_detection.ActivityManager
	nm network.Manager

	router *httprouter.Router

	// chans to receive updates from the network manager
	updateCh    chan network.Message
	updateReqCh chan network.Message

	// pullPeriod time.Duration
	pushFanout int

	// If SessionManager is monitored,
	isMonitored bool
	// monitorLines contains the lines to send to monitoring
	monitorLines [][]string
	monitorMutex sync.Mutex

	// Save the most recent Timestamp of pending updates
	//pendingUpdateTs time.Time
}

func NewSessionManager(nm network.Manager, am activity_detection.ActivityManager,
	myself gossip.Descriptor, isMonitored bool, pushFanout int /*, pullPeriod int*/) *SessionManager {

	sm := new(SessionManager)

	sm.am = am
	sm.nm = nm
	sm.myself = myself

	sm.isMonitored = isMonitored
	sm.pushFanout = pushFanout

	// sm.pullPeriod = time.Duration(pullPeriod) * time.Millisecond

	// Creation of the chans
	updateCh := make(chan network.Message)
	updateReqCh := make(chan network.Message)
	// Register chans on the network Manager
	sm.nm.RegisterObserver(network.MessageType("SESSION_UPDATE"), updateCh)
	// sm.nm.RegisterObserver(network.MessageType("SESSION_UPDATE_REQ"), updateReqCh)
	// Save receive chan in am object
	sm.updateCh = updateCh
	sm.updateReqCh = updateReqCh

	//debugSm("SessionManager initialised with a period of %vs.\n", sm.pullPeriod.Seconds())
	debugSm("SessionManager initialised.\n")

	// In another goroutine because http.ListenAndServe is blocking
	go sm.startRestRouter()
	go sm.loop()

	return sm
}

// We only add an ActivitySession to our sequence when it was our device that was used
// The rest of the time, we just try to get the newest user session from gossip
func (sm *SessionManager) updateSession(ts time.Time, session string) {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	if sm.userSession.Timestamp.After(ts) ||
		sm.userSession.Timestamp.Equal(ts) {
		fmt.Fprintf(os.Stderr, "[SessionManager.updateSession] New timestamp is older than mine... Screw it.\n")
		return
	}

	debugSm("[SessionManager.updateSession] User created new session at time %q:\n%q\n\n",
		ts, session)

	sm.userSession = UserSession{
		Session:    session,
		Timestamp:  ts,
		DeviceUsed: sm.myself,
	}

	go sm.am.AddRound(ts, sm.myself)

	go sm.pushUpdate(sm.userSession)

	if sm.isMonitored {
		sm.monitorMutex.Lock()
		sm.monitorLines = append(sm.monitorLines,
			[]string{
				sm.myself.ID(),                            // My ID
				sm.myself.GetAddr(),                       // My address
				ts.Format(activity_detection.TIME_LAYOUT), // Timestamp (equals to SessionTimestamp)
				sm.myself.ID(),                            // Sender of the session (ourself)
				ts.Format(activity_detection.TIME_LAYOUT), // SessionTimestamp
				sm.myself.ID()})                           // Device used (ourself)
		sm.monitorMutex.Unlock()
	}
}

func (sm *SessionManager) loop() {
	//pullTicker := time.NewTicker(sm.pullPeriod)
	//defer pullTicker.Stop()

	for {
		select {
		// case <-pullTicker.C:
		// 	go sm.pullUpdate()
		case mess := <-sm.updateCh:
			go sm.receiveUpdate(mess)
			// case mess := <-sm.updateReqCh:
			// 	go sm.handleUpdateRequest(mess)
		}
	}
}

func (sm *SessionManager) pushUpdate(s UserSession) {
	if s.Timestamp.IsZero() {
		return
	}

	peers := sm.am.SelectPeers(sm.pushFanout)

	fmt.Printf("Pushing update to following: %v\n", peers)
	for _, d := range peers {
		go gossip.EncodeAndSend(
			SessionMessage{
				Sender:  sm.myself,
				Session: s,
			},
			d.GetAddr(), sm.nm, network.MessageType("SESSION_UPDATE"))
	}
}

func (sm *SessionManager) receiveUpdate(mess network.Message) {
	x := gossip.Decode(mess, network.MessageType("SESSION_UPDATE"))
	if x == nil {
		fmt.Fprintf(os.Stderr,
			"[SessionManager.receiveUpdate] Failed decoding payload.\n")
		return
	}
	payload := x.(SessionMessage)

	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	// If the remote has an older session than I, we do nothing
	if payload.Session.Timestamp.IsZero() ||
		payload.Session.Timestamp.Before(sm.userSession.Timestamp) ||
		payload.Session.Timestamp.Equal(sm.userSession.Timestamp) {
		return
	}

	debugSm("[SessionManager.receiveUpdate] Updating session from %q to %q following SESSION_UPDATE from %q:\n%q\n\n",
		sm.userSession.Timestamp.Format(activity_detection.TIME_LAYOUT),
		payload.Session.Timestamp.Format(activity_detection.TIME_LAYOUT),
		payload.Sender.ID(), payload.Session.Session)

	sm.userSession = payload.Session

	if sm.isMonitored {
		sm.monitorMutex.Lock()
		sm.monitorLines = append(sm.monitorLines,
			[]string{
				sm.myself.ID(),                                                  // My ID
				sm.myself.GetAddr(),                                             // My address
				time.Now().Format(activity_detection.TIME_LAYOUT),               // Timestamp (now)
				payload.Sender.ID(),                                             // Sender of the session (ourself)
				sm.userSession.Timestamp.Format(activity_detection.TIME_LAYOUT), // Session Timestamp
				sm.userSession.DeviceUsed.ID()})                                 // Device used
		sm.monitorMutex.Unlock()
	}

}

// func (sm *SessionManager) handleUpdateRequest(mess network.Message) {
// 	x := gossip.Decode(mess, network.MessageType("SESSION_UPDATE_REQ"))
// 	if x == nil {
// 		fmt.Fprintf(os.Stderr,
// 			"[SessionManager.handleUpdateRequest] Failed decoding payload.\n")
// 		return
// 	}
// 	payload := x.(SessionMessage)

// 	sm.mutex.RLock()
// 	defer sm.mutex.RUnlock()

// 	// If the remote has a more recent session than I, don't bother answering
// 	if sm.userSession.Timestamp.IsZero() ||
// 		payload.Session.Timestamp.After(sm.userSession.Timestamp) ||
// 		payload.Session.Timestamp.Equal(sm.userSession.Timestamp) {
// 		return
// 	}

// 	debugSm("[SessionManager.handleUpdateRequest] Sending our session to %q following its SESSION_UPDATE_REQ.\n\n",
// 		payload.Sender)

// 	go gossip.EncodeAndSend(
// 		SessionMessage{
// 			Sender:  sm.myself,
// 			Session: sm.userSession,
// 		},
// 		payload.Sender.GetAddr(), sm.nm, network.MessageType("SESSION_UPDATE"))
// }

// func (sm *SessionManager) pullUpdate() {
// 	sm.mutex.RLock()
// 	defer sm.mutex.RUnlock()

// 	before := sm.am.GetBeforeSet()

// 	if len(before) == 0 {
// 		fmt.Fprintf(os.Stderr, "[SessionManager.pullUpdate] Empty BeforeSet, can't pull!\n")
// 		return
// 	}

// 	debugSm("[SessionManager.pullUpdate] Pulling session from %v peers...\n",
// 		len(before))

// 	for _, d := range before {
// 		debugSm("[SessionManager.pullUpdate] Pulling session from %q.\n", d.ID())
// 		go gossip.EncodeAndSend(
// 			SessionMessage{
// 				Sender: sm.myself,
// 				// We only send the timestamp, not the session nor the device used
// 				Session: UserSession{Timestamp: sm.userSession.Timestamp},
// 			},
// 			d.GetAddr(), sm.nm, network.MessageType("SESSION_UPDATE_REQ"))
// 	}
// }

func (sm *SessionManager) startRestRouter() {
	// We use "github.com/julienschmidt/httprouter" to create our REST API
	sm.router = httprouter.New()

	sm.router.GET(SESSION_REST_PATH, func(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {

		sm.mutex.RLock()
		// Will be marshaled and sent to the client
		s := SessionJSON{
			Timestamp:  sm.userSession.Timestamp,
			Session:    sm.userSession.Session,
			Sender:     sm.myself.ID(),
			DeviceUsed: sm.userSession.DeviceUsed.ID(),
		}
		sm.mutex.RUnlock()

		// We always speak JSON
		w.Header().Set("Content-Type", "application/json")
		// Allow cross domain JS requests
		w.Header().Set("Access-Control-Allow-Origin", "*")

		if sj, err := json.Marshal(s); err != nil {
			w.WriteHeader(500)
			fmt.Fprintf(w,
				"{\"error\":\"I failed encoding my session to JSON, mate.\"}\n")
		} else {
			w.WriteHeader(200)
			fmt.Fprintf(w, "%s", sj)
		}
	})

	sm.router.PUT(SESSION_REST_PATH, func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		s := SessionJSON{}

		// We always speak JSON
		w.Header().Set("Content-Type", "application/json")
		// Allow cross domain JS requests
		w.Header().Set("Access-Control-Allow-Origin", "*")

		if err := json.NewDecoder(r.Body).Decode(&s); err != nil {
			w.WriteHeader(400)
			fmt.Fprintf(w,
				"{\"error\": \"I failed decoding you JSON, bro.\", "+
					"\"error_content\": %q}\n", err)
		} else {
			if s.Timestamp.IsZero() {
				s.Timestamp = time.Now().Round(0)
			}
			// if len(s.Sender) == 0 {
			// 	s.Sender = sm.myself.GetAddr()
			// }
			sm.updateSession(s.Timestamp, s.Session)

			w.WriteHeader(200)
			fmt.Fprintf(w,
				"{\"info\": \"All right, I updated my session, pal.\", \"Timestamp\": \"%v\", \"DeviceUsed\": \"%v\", \"DeviceAddress\":\"%v\"}\n",
				s.Timestamp.Format(activity_detection.TIME_LAYOUT), sm.myself.ID(), sm.myself.GetAddr())
		}
	})

	// Blocking function
	http.ListenAndServe(":"+HTTP_PORT, sm.router)
}

func debugSm(format string, a ...interface{}) {
	if DEBUG_SM {
		fmt.Printf(format, a...)
	}
}
