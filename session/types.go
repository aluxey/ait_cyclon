package session

import (
	"ait_cyclon/gossip"
	//"ait_cyclon/gossip/activity_detection"
	"time"
)

// // Two packet types :
// //		"REQ": request someone else's session info
// //		"ANS": give your session info to the one who sent "REQ"
// type PktType string

// // The TCP packet sent to communicate session information between nodes
// type Packet struct {
// 	Type      PktType
// 	Timestamp time.Time
// 	Session   string
// 	Sender    gossip.Descriptor
// }

type UserSession struct {
	Session    string
	Timestamp  time.Time
	DeviceUsed gossip.Descriptor
}

type SessionMessage struct {
	Sender  gossip.Descriptor
	Session UserSession
}

// The JSON type used to communicate session info between the REST lient and us
type SessionJSON struct {
	// The session's timestamp (not the current time)
	// You can omit it when PUTting, I will create a timestamp at "time.Now()"
	Timestamp  time.Time `json:"timestamp,omitempty"`
	Session    string    `json:"session"`
	Sender     string    `json:"sender"`
	DeviceUsed string    `json:"device_used"`
}
