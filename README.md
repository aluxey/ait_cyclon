# Sprinkler: A probabilistic dissemination protocol to provide fluid user interaction in multi-device ecosystems

Code for the [Sprinkler paper](https://hal.inria.fr/view/index/docid/1797537) by Luxey et al.

The repository and code are in a ciritical state, sorry for that. Nevertheless, the project's code, along with the deployment code, work, so you should be able to navigate through it.

## Installation

The code was only tested on Linux. It should work on Mac too. If you only have Windows, come back when you own an operating system.

You need:

* Golang installed and configured: [GOPATH and such](https://golang.org/doc/code.html#Organization)
* Docker
* Python3

### Python dependencies

You need a handful of modules alongside Python to deploy the project (*please add any forgotten or new ones to this list and to `python/requirements.txt`*):

* numpy
* pandas
* matplotlib
* networkx
* jupyter 
* docker

Instead of installing these modules sytem-wide, I recommend you use [virtualenv](https://virtualenv.pypa.io/en/latest/). Do the following to install the dependencies;

	# from the /python/ directory:
	virtualenv env 
	source env/bin/activate 
	pip install -r requirements.txt 


This will install any dependencies in `/python/env`. You should now be up and rolling.

## Deployment

The deployment is made through [Jupyter](https://jupyter.org/). You must load the virtualenv environment on the terminal pane before launching Jupyter as follows:

	# from the /python directory:
	source env/bin/activate # Loads the environment
	jupyter notebook # Starts Jupyter 

You will be redirected to a webpage listing files in the `/python` directory. Open `Holistic simulations.ipynb`. This file loads code from `/python/my_modules`, and will:

1. Start experiments using several user behavioral models 
2. Retrieve experiments results and plot some figures 

If you don't know what is Jupyter, give it a go, it is very useful. The main idea is that you run segments of code called *cells* (using `<Shift> + <Enter>`) at a time, but keep your memory stack between cell executions. This is incredibly useful to load datasets only once, and play around with them endlessly.

For that reason, some parts of the code below the "Plotting" header are random garbage I used to investigate experimental results. Do not pay too much attention to it, unless you want to reproduce my figures.

**Beware, it is very easy to override previous output if you are not careful.** First thing you should do is change the `experiment_name` parameter in the `experiment_parameters` dictionary to generate results in a new directory (under `/experimental_output/<experiment_name>`) every time you start a new experiment and want to keep your previous output.

### Sprinkler command line parameters

Bits of the `experiment_parameters` dict (from `/python/Holistic simulations.ipynb`):

    experiment_parameters=dict(
        experiment_name="push_updates",

        # [...]

        container=dict(
            name="ait_exp{experiment_id}_{container_id}",
            label="experiment{experiment_id}",
            ip="13.{experiment_id}.0.{container_id}",
            only_stop=True,
            arguments="-net={net_addr} -name={name} -m={monitor_dir} -sequence_json={json_path} \
            -session_push_fanout={session_push_fanout} -ad_fanout={activity_fanout} \
            -msession -mactivity -mnet "
        ),
         # [...]
        
        program_parameters=dict(
            session_push_fanout=2,
            activity_fanout=4,
        )
    )

Then, in `/python/my_modules/experiment.py`:

    cont_args = experiment_parameters['container']['arguments'].format(
        net_addr=network['addr'],
        monitor_dir=volumes['monitor']['cont_dir'],
        json_path=volumes['json']['cont_dir']+json_fn,
        name=containers_name[device_id], bs_addr=bs_addr,
        **experiment_parameters['program_parameters'])

## Overall code organization 

There are two parts: the project's code, in Go; and the deployment code, in Python. Some Shell scripts lie around, notably to build the project in a lightweight Docker container.

The Go project lives at the repository's root (cf `/main.go`), and has submodules (`monitoring`, `network`, `gossip`, `session`). It uses a (crappy) vendoring system for the dependencies, which you hopefully do not have to care about. (To my discharge, Go has known countless vendoring systems and none are good at the time of writing. I recommend Makefiles, after all.)

The Python code lives in `/python`, is constituted of Jupyter notebooks and of a subdirectory `/python/my_modules`. 

## Project characteristics

* The **REST TCP port is 10338**; 
* The **URL is /session**;
* **The session can only hold strings (not binary data)**;
* You communicate with the API through the `SessionJSON struct` defined in `ait/types.go`, in JSON.

## Test drive 

Given two running containers at IPs `13.37.0.2` and `13.37.0.3`, you can try out the system with `curl`:

	$ curl -X GET 13.37.0.2:10338/session
	{"timestamp":"0001-01-01T00:00:00Z","session":""}

	$ curl -X PUT -d '{"session": "foo"}' 13.37.0.2:10338/session
	{'info': 'All right, I updated my session, pal.'}
	$ curl -X GET 13.37.0.2:10338/session
	{"timestamp":"2017-04-05T13:11:43.830111999Z","session":"foo"}
	$ curl -X GET 13.37.0.3:10338/session                       
	{"timestamp":"2017-04-05T13:11:43.830111999Z","session":"foo"}

	$  curl -X PUT -d '{"session": "bar"}' 13.37.0.3:10338/session
	{'info': 'All right, I updated my session, pal.'}
	$ curl -X GET 13.37.0.3:10338/session                        
	{"timestamp":"2017-04-05T13:12:55.587053303Z","session":"bar"}
	$ curl -X GET 13.37.0.2:10338/session
	{"timestamp":"2017-04-05T13:12:55.587053303Z","session":"bar"}

---

Adrien Luxey 

Last update: May 2019