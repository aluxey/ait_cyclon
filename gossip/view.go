package gossip

import (
	"fmt"
)

const (
	ID_SEPARATOR = "|"
)

// View is a set of Descriptors
// Sets (like other generics) don't exist in Go, so we use a map of Descriptors
// The map's key is the Desc.ID()
type View map[string]Descriptor

func NewView(items ...Descriptor) *View {
	var v View = make(map[string]Descriptor)
	v.Add(items...)
	return &v
}

func UnionView(views ...*View) *View {
	v := NewView()
	for _, view := range views {
		if view == nil {
			//fmt.Fprintf(os.Stderr, "[view.UnionView] nil item in views list!\n")
			continue
		}
		v.Add(view.GetDescriptors()...)
	}

	return v
}

// Add items to the View without replacing existing items
func (v *View) Add(items ...Descriptor) {
	for _, item := range items {
		if item == nil {
			//fmt.Fprintf(os.Stderr, "[view.Add] nil item in descriptors list!\n")
			continue
		}
		// Do not replace duplicates
		if _, ok := (*v)[item.ID()]; ok {
			continue
		}
		(*v)[item.ID()] = item.Copy()
	}
}

func (v *View) Clear() {
	for k := range *v {
		delete(*v, k)
	}
}
func (v *View) Copy() *View {
	var newView View = make(map[string]Descriptor)
	for key, item := range *v {
		newView[key] = item.Copy()
	}
	return &newView
}
func (v *View) Diff(v2 *View) {
	for key2 := range *v2 {
		delete(*v, key2)
	}
}
func (v *View) Equals(v2 *View) bool {
	// For testing
	if len(*v) != len(*v2) {
		return false
	}
	for key, item := range *v {
		if item2, ok := (*v2)[key]; ok {
			if !item.Equals(item2) {
				return false
			}
		} else {
			return false
		}
	}
	return true
}
func (v *View) Get(key string) Descriptor {
	// For testing
	if item, ok := (*v)[key]; ok {
		return item.Copy()
	}
	return nil
}
func (v *View) GetDescriptors() []Descriptor {
	list := make([]Descriptor, len(*v))
	i := 0
	for _, item := range *v {
		list[i] = item.Copy()
		i++
	}
	return list
}
func (v *View) GetIDs() string {
	ret := ""
	i := 0
	for key := range *v {
		ret += key
		if i < len(*v)-1 {
			ret += ID_SEPARATOR
		}
		i++
	}
	return ret
}
func (v *View) GetOldest() Descriptor {
	oldestAge := 0
	oldestKey := ""
	for key, item := range *v {
		if item.GetAge() >= oldestAge {
			oldestKey = key
			oldestAge = item.GetAge()
		}
	}
	return (*v)[oldestKey].Copy()
}
func (v *View) Has(item Descriptor) bool {
	_, ok := (*v)[item.ID()]
	return ok
}
func (v *View) IncrementAge() {
	for _, item := range *v {
		item.IncrementAge()
	}
}
func (v *View) IsEmpty() bool {
	return len(*v) == 0
}
func (v *View) Len() int {
	return len(*v)
}
func (v *View) Merge(views ...*View) {
	for _, view := range views {
		v.Add(view.GetDescriptors()...)
	}
}
func (v *View) Pop() Descriptor {
	for key, _ := range *v {
		defer delete(*v, key)
		return (*v)[key]
	}
	return nil
}
func (v *View) Remove(items ...Descriptor) {
	for _, item := range items {
		if item != nil {
			delete(*v, item.ID())
		}
		// If NONE_TOPOLOGY, item doesn't have a Profile, hence no ID
		// We can't import gossip/topology from gossip: include cycle
		// if item.GetType() == TopologyType("none") {
		// 	continue
		// }

	}
}

// func (v *View) ResetAge() {
// 	for key := range *v {
// 		(*v)[key] = (*v)[key].ResetAge()
// 	}
// }
func (v *View) String() string {
	ret := fmt.Sprintf("View(len:%v){", len(*v))
	for _, item := range *v {
		ret += fmt.Sprintf("\n\t%s,", item)
	}
	ret += "}"
	return ret
}

// Untested in view_test.go
// func (v *View) KeepOnlyShape(shapeName string) {
// 	for key, item := range *v {
// 		if item.GetShapeName() != shapeName {
// 			delete(*v, key)
// 		}
// 	}
// }
// func (v *View) RemoveShape(shapeName string) {
// 	for key, item := range *v {
// 		if item.GetShapeName() == shapeName {
// 			delete(*v, key)
// 		}
// 	}
// }
