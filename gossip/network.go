package gossip

import (
	"ait_cyclon/network"
	"bytes"
	"encoding/gob"
	"fmt"
	"os"
	//"reflect"
	//"errors"
	// "math"
	//"net"
	//"sort"
)

type Payload struct {
	Sender Descriptor
	V      *View
}

func EncodeAndSend(x interface{}, recipientAddr string,
	nm network.Manager, messType network.MessageType) {

	// Create a buffer to encode the payload into
	var encodedPayload bytes.Buffer
	enc := gob.NewEncoder(&encodedPayload)
	if err := enc.Encode(&x); err != nil {
		fmt.Fprintf(os.Stderr, "[gossip.EncodeAndSend] Error while encoding payload: %v\n", err)
		return
	}

	// fmt.Printf("[utils.EncodeAndSend] Sending following view of type %v to %v:\n%+v\n",
	// 	messType, recipientAddr, payload.V)

	if err := nm.Send(network.Message{messType, encodedPayload.Bytes()}, recipientAddr); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func Decode(mess network.Message, expectedType network.MessageType) interface{} {
	// Little assertion to be sure we did things right
	if mess.Type != expectedType {
		fmt.Fprintf(os.Stderr,
			"[gossip.Decode] Wrong datagram type received: expected %v, got %v .\n",
			expectedType, mess.Type)
		return nil
	}

	var decoded interface{}
	decoderBuf := bytes.NewBuffer(mess.Payload)
	dec := gob.NewDecoder(decoderBuf)
	if err := dec.Decode(&decoded); err != nil {
		fmt.Fprintf(os.Stderr, "[gossip.Decode] Error while decoding payload: %v\n", err)
		return nil
	}

	// fmt.Printf("[utils.Decode] Received following view of type %v from %v:\n%+v\n",
	// 	mess.Type, payload.Sender.GetAddr(), payload.V)

	return decoded
}
