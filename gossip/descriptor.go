package gossip

import (
	"fmt"
	"math/rand"
)

const (
	CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	ID_SIZE    = 10
)

// ------------------------- Descriptor interface -------------------------
type Descriptor interface {
	ID() string
	IDString() string
	GetAddr() string

	// Equals compares addresses
	Equals(Descriptor) bool

	// Age stuff
	GetAge() int
	IncrementAge()
	ResetAge()
	Younger(Descriptor) bool

	Copy() Descriptor
	String() string
}

// ------------------------- BaseDescriptor struct -------------------------
type BaseDescriptor struct {
	MyID string

	// "IP:PORT"
	Addr string

	// Exporting the Age eases the deletion of stale descriptors in Cyclon
	Age int
}

func NewBaseDescriptor(myID, addr string) Descriptor {
	if myID == "" {
		myID = RandomID()
	}
	return &BaseDescriptor{
		MyID: myID,
		Addr: addr,
		Age:  0,
	}
}

func (d *BaseDescriptor) ID() string                 { return d.MyID }
func (d *BaseDescriptor) IDString() string           { return d.MyID }
func (d *BaseDescriptor) GetAddr() string            { return d.Addr }
func (d *BaseDescriptor) Equals(d1 Descriptor) bool  { return d.Addr == d1.GetAddr() }
func (d *BaseDescriptor) GetAge() int                { return d.Age }
func (d *BaseDescriptor) IncrementAge()              { d.Age++ }
func (d *BaseDescriptor) ResetAge()                  { d.Age = 0 }
func (d *BaseDescriptor) Younger(d1 Descriptor) bool { return d.Age < d1.GetAge() }
func (d *BaseDescriptor) String() string {
	return fmt.Sprintf("Descriptor(%p){ID: %v, Addr: %v, Age: %d}", d, d.MyID, d.Addr, d.Age)
}
func (d *BaseDescriptor) Copy() Descriptor {
	var d1 BaseDescriptor = *d
	return &d1
}

// ------------------------- Helper functions -------------------------
func RandomID() (b string) {
	for i := 0; i < ID_SIZE; i++ {
		b += string(CHARACTERS[rand.Int63()%int64(len(CHARACTERS))])
	}
	return
}
