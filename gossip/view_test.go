package gossip

import (
	"testing"
)

var SampleDescriptors []Descriptor = []Descriptor{
	OneDDescriptor{
		BaseDescriptor: BaseDescriptor{
			Addr: "1",
			Age:  1,
			P:    OneDProfile{1},
			Type: "test",
		},
		distance: 0,
	},
	OneDDescriptor{
		BaseDescriptor: BaseDescriptor{
			Addr: "2",
			Age:  2,
			P:    OneDProfile{2},
			Type: "test",
		},
		distance: 0,
	},
	OneDDescriptor{
		BaseDescriptor: BaseDescriptor{
			Addr: "3",
			Age:  3,
			P:    OneDProfile{3},
			Type: "test",
		},
		distance: 0,
	},
	OneDDescriptor{
		BaseDescriptor: BaseDescriptor{
			Addr: "4",
			Age:  4,
			P:    OneDProfile{4},
			Type: "test",
		},
		distance: 0,
	},
	OneDDescriptor{
		BaseDescriptor: BaseDescriptor{
			Addr: "5",
			Age:  5,
			P:    OneDProfile{5},
			Type: "test",
		},
		distance: 0,
	},
}
var NewDesc Descriptor = OneDDescriptor{
	BaseDescriptor: BaseDescriptor{
		Addr: "6",
		Age:  0,
		P:    OneDProfile{6},
		Type: "test",
	},
	distance: 0,
}

func TestNewView(t *testing.T) {
	V := NewView(SampleDescriptors...)

	if len(*V) != len(SampleDescriptors) {
		t.Errorf("TestNewView failed:\nV: %v\nSampleDescriptors: %v\nThey don't have the same length.\n\n",
			V, SampleDescriptors)
	}

	for _, desc := range SampleDescriptors {
		found := false
		for key, item := range *V {
			if key == desc.ID() && item.Equals(desc) {
				found = true
				break
			}
		}

		if !found {
			t.Errorf("TestNewView failed:\nV: %v\nSampleDescriptors: %v\nV doesn't contain: %v\n\n",
				V, SampleDescriptors, desc)
		}
	}
}

func TestUnionViewDistinct(t *testing.T) {
	V := UnionView(NewView(SampleDescriptors...), NewView(NewDesc))

	ShouldBe := append(SampleDescriptors, NewDesc)

	if V.Len() != len(ShouldBe) {
		t.Errorf("TestUnionViewDistinct failed:\nV: %v\nShouldBe: %v\nThey don't have the same length.\n\n",
			V, ShouldBe)
		return
	}

	for _, sample := range ShouldBe {
		found := false
		for _, desc := range V.GetDescriptors() {
			if desc.Equals(sample) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestUnionViewDistinct failed:\nV: %v\nShouldBe: %v\nV doesn't contain: %v\n\n",
				V, ShouldBe, sample)
		}
	}
}
func TestUnionViewOverlapping(t *testing.T) {
	V := UnionView(NewView(SampleDescriptors...), NewView(NewDesc, SampleDescriptors[0], SampleDescriptors[1]))

	ShouldBe := append(SampleDescriptors, NewDesc)

	if V.Len() != len(ShouldBe) {
		t.Errorf("TestUnionViewOverlapping failed:\nV: %v\nShouldBe: %v\nThey don't have the same length.\n\n",
			V, ShouldBe)
		return
	}

	for _, sample := range ShouldBe {
		found := false
		for _, desc := range V.GetDescriptors() {
			if desc.Equals(sample) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestUnionViewOverlapping failed:\nV: %v\nShouldBe: %v\nV doesn't contain: %v\n\n",
				V, ShouldBe, sample)
		}
	}
}

// func TestAddNewDesc(t *testing.T) {
// 	//NewDesc := RingDescriptor{"6", Profile{6}, 6}

// 	V := NewView(SampleDescriptors...)
// 	V.Add(NewDesc)

// 	ComparedDescriptors := []Descriptor{
// 		RingDescriptor{"1", Profile{1}, 1},
// 		RingDescriptor{"2", Profile{2}, 2},
// 		RingDescriptor{"3", Profile{3}, 3},
// 		RingDescriptor{"4", Profile{4}, 4},
// 		RingDescriptor{"5", Profile{5}, 5},
// 		RingDescriptor{"6", Profile{6}, 6},
// 	}
// 	ComparedView := NewView(ComparedDescriptors...)

// 	if !V.Equals(ComparedView) {
// 		t.Errorf("TestAddNewDesc failed:\nV: %v\nComparedView: %v\n\n", V, ComparedView)
// 	}
// }

// func TestAddBetterDesc(t *testing.T) {
// 	NewDesc := RingDescriptor{"5", Profile{5}, 6}

// 	V := NewView(SampleDescriptors...)
// 	V.Add(NewDesc)

// 	ComparedDescriptors := []Descriptor{
// 		RingDescriptor{"1", Profile{1}, 1},
// 		RingDescriptor{"2", Profile{2}, 2},
// 		RingDescriptor{"3", Profile{3}, 3},
// 		RingDescriptor{"4", Profile{4}, 4},
// 		RingDescriptor{"5", Profile{5}, 6},
// 	}

// 	ComparedView := NewView(ComparedDescriptors...)

// 	if !V.Equals(ComparedView) {
// 		t.Errorf("TestAddNewDesc failed:\nV: %v\nComparedView: %v\n\n", V, ComparedView)
// 	}
// }

func TestAddOlderDesc(t *testing.T) {
	NewDesc := OneDDescriptor{
		BaseDescriptor: BaseDescriptor{
			Addr: "5",
			Age:  6,
			P:    OneDProfile{5},
			Type: "test",
		},
		distance: 0,
	}

	V := NewView(SampleDescriptors...)
	V.Add(NewDesc)

	ComparedView := NewView(SampleDescriptors...)

	if !V.Equals(ComparedView) {
		t.Errorf("TestAddNewDesc failed:\nV: %v\nComparedView: %v\n\n", V, ComparedView)
	}
}

func TestCopy(t *testing.T) {
	V := NewView(SampleDescriptors...)
	ComparedView := V.Copy()

	if !V.Equals(ComparedView) {
		t.Errorf("TestCopy failed:\nV: %v\nComparedView: %v\n\n", V, ComparedView)
	}
}

func TestEqualsSameViews(t *testing.T) {
	V := NewView(SampleDescriptors...)
	ComparedView := NewView(SampleDescriptors...)

	if !V.Equals(ComparedView) {
		t.Errorf("TestEqualsSameViews failed:\nV: %v\nComparedView: %v\n\n", V, ComparedView)
	}
}
func TestEqualsDifferentViews(t *testing.T) {
	V := NewView(SampleDescriptors...)
	ComparedView := NewView(append(SampleDescriptors, NewDesc)...)

	if V.Equals(ComparedView) {
		t.Errorf("TestEqualsDifferentViews failed:\nV: %v\nComparedView: %v\n\n", V, ComparedView)
	}
}

func TestGetExisting(t *testing.T) {
	V := NewView(SampleDescriptors...)
	got := V.Get(SampleDescriptors[0].ID())

	if !got.Equals(SampleDescriptors[0]) {
		t.Errorf("TestGetExisting failed:\nV: %v\nGet(%v): %v\nShould be:%v\n\n",
			V, SampleDescriptors[0].ID(), got, SampleDescriptors[0])
	}
}
func TestGetNonExisting(t *testing.T) {
	V := NewView(SampleDescriptors...)
	ID := "azuekg"
	Got := V.Get(ID)

	if Got != nil {
		t.Errorf("TestGetNonExisting failed:\nV: %v\nGet(%v): %v\nShould be:%v\n\n",
			V, ID, Got, nil)
	}
}

func TestGetDescriptors(t *testing.T) {
	V := NewView(SampleDescriptors...)
	Descriptors := V.GetDescriptors()

	if V.Len() != len(SampleDescriptors) {
		t.Errorf("TestGetDescriptors failed:\nV: %v\nShouldBe: %v\nThey don't have the same length.\n\n",
			V, SampleDescriptors)
		return
	}

	for _, sample := range SampleDescriptors {
		found := false
		for _, desc := range Descriptors {
			if desc.Equals(sample) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestGetDescriptors failed:\nV: %v\nSampleDescriptors: %v\nV doesn't contain: %v\n\n",
				V, SampleDescriptors, sample)
		}
	}
}

func TestGetOldest(t *testing.T) {
	var OldestDesc Descriptor = OneDDescriptor{
		BaseDescriptor: BaseDescriptor{
			Addr: "100",
			Age:  100,
			P:    OneDProfile{100},
			Type: "test",
		},
		distance: 0,
	}
	V := NewView(append(SampleDescriptors, OldestDesc)...)
	OldestInV := V.GetOldest()

	if !OldestDesc.Equals(OldestInV) {
		t.Errorf("TestGetOldest failed:\nV: %v\nOldest is: %v\nV.GetOldest() returned: %v\n\n",
			V, OldestDesc, OldestInV)
	}
}

func TestHasExisting(t *testing.T) {
	V := NewView(SampleDescriptors...)
	Elem := SampleDescriptors[0]
	Has := V.Has(Elem)

	if !Has {
		t.Errorf("TestHasExisting failed:\nV: %v\nBuilt from: %v\nV.Has(%v) returned false.%v\n\n",
			V, SampleDescriptors, Elem, Has)
	}
}
func TestHasNonExisting(t *testing.T) {
	V := NewView(SampleDescriptors...)
	Has := V.Has(NewDesc)

	if Has {
		t.Errorf("TestHasNonExisting failed:\nV: %v\nBuilt from: %v\nV.Has(%v) returned false.%v\n\n",
			V, SampleDescriptors, NewDesc, Has)
	}
}

func TestIncrementAge(t *testing.T) {
	V := NewView(SampleDescriptors[0])
	var Compared Descriptor = SampleDescriptors[0].IncrementAge()

	V.IncrementAge()

	if !V.Get(Compared.ID()).Equals(Compared) {
		t.Errorf("TestIncrementAge failed:\nV: %v\nDiffers from Compared: %v\n\n",
			V, Compared)
	}
}

func TestIsEmptyOnEmpty(t *testing.T) {
	V := NewView()

	if !V.IsEmpty() {
		t.Errorf("TestIsEmptyOnEmpty failed:\nEmpty view V: %v\nReturned V.IsEmpty() == false\n\n", V)
	}
}
func TestIsEmptyOnNonEmpty(t *testing.T) {
	V := NewView(SampleDescriptors...)

	if V.IsEmpty() {
		t.Errorf("TestIsEmptyOnNonEmpty failed:\nEmpty view V: %v\nReturned V.IsEmpty() == true\n\n", V)
	}
}

// No need to test Len, I think...

func TestMergeDistinct(t *testing.T) {
	V := NewView(SampleDescriptors...)
	V.Merge(NewView(NewDesc))

	ShouldBe := append(SampleDescriptors, NewDesc)

	if V.Len() != len(ShouldBe) {
		t.Errorf("TestMergeDistinct failed:\nV: %v\nShouldBe: %v\nThey don't have the same length.\n\n",
			V, ShouldBe)
		return
	}

	for _, sample := range ShouldBe {
		found := false
		for _, desc := range V.GetDescriptors() {
			if desc.Equals(sample) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestMergeDistinct failed:\nV: %v\nShouldBe: %v\nV doesn't contain: %v\n\n",
				V, ShouldBe, sample)
		}
	}
}
func TestMergeOverlapping(t *testing.T) {
	V := NewView(SampleDescriptors...)
	V.Merge(NewView(NewDesc, SampleDescriptors[0], SampleDescriptors[1]))

	ShouldBe := append(SampleDescriptors, NewDesc)

	if V.Len() != len(ShouldBe) {
		t.Errorf("TestMergeOverlapping failed:\nV: %v\nShouldBe: %v\nThey don't have the same length.\n\n",
			V, ShouldBe)
		return
	}

	for _, sample := range ShouldBe {
		found := false
		for _, desc := range V.GetDescriptors() {
			if desc.Equals(sample) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestMergeOverlapping failed:\nV: %v\nShouldBe: %v\nV doesn't contain: %v\n\n",
				V, ShouldBe, sample)
		}
	}
}

// PrintProfiles not tested

func TestRemoveExisting(t *testing.T) {
	V := NewView(SampleDescriptors...)
	V.Remove(SampleDescriptors[:2]...)

	ShouldBe := SampleDescriptors[2:]

	if V.Len() != len(ShouldBe) {
		t.Errorf("TestRemoveExisting failed:\nV: %v\nShouldBe: %v\nThey don't have the same length.\n\n",
			V, ShouldBe)
		return
	}

	for _, sample := range ShouldBe {
		found := false
		for _, desc := range V.GetDescriptors() {
			if desc.Equals(sample) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestRemoveExisting failed:\nV: %v\nShouldBe: %v\nV doesn't contain: %v\n\n",
				V, ShouldBe, sample)
		}
	}
}
func TestRemoveNonExisting(t *testing.T) {
	V := NewView(SampleDescriptors...)
	V.Remove(NewDesc)

	ShouldBe := SampleDescriptors

	if V.Len() != len(ShouldBe) {
		t.Errorf("TestRemoveNonExisting failed:\nV: %v\nShouldBe: %v\nThey don't have the same length.\n\n",
			V, ShouldBe)
		return
	}

	for _, sample := range ShouldBe {
		found := false
		for _, desc := range V.GetDescriptors() {
			if desc.Equals(sample) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestRemoveNonExisting failed:\nV: %v\nShouldBe: %v\nV doesn't contain: %v\n\n",
				V, ShouldBe, sample)
		}
	}
}
func TestResetAge(t *testing.T) {
	V := NewView(SampleDescriptors...)
	V.ResetAge()

	for _, desc := range V.GetDescriptors() {
		if desc.GetAge() != 0 {
			t.Errorf("TestResetAge failed:\nV: %v\nItem %v has an age different from zero.\n\n",
				V, desc)
		}
	}
}

func TestGetSubsetSmaller(t *testing.T) {
	V := NewView(SampleDescriptors...)
	L := 2
	Subset := V.GetSubset(L)

	if Subset.Len() != L {
		t.Errorf("TestGetSubsetSmaller failed:\nV: %v\nSubset: %v\nL: %v\nSubset has a wrong size (%v).\n\n",
			V, Subset, L, Subset.Len())
		return
	}

	for _, sub := range Subset.GetDescriptors() {
		found := false
		for _, desc := range V.GetDescriptors() {
			if desc.Equals(sub) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("TestGetSubsetSmaller failed:\nV: %v\nSubset: %v\nV doesn't contain: %v\n\n",
				V, Subset, sub)
		}
	}
}
func TestGetSubsetBigger(t *testing.T) {
	V := NewView(SampleDescriptors...)
	L := 10
	Subset := V.GetSubset(L)

	if V.Len() != len(SampleDescriptors) {
		t.Errorf("TestGetSubsetBigger failed:\nV: %v\nSubset: %v\nL: %v\nSubset has a wrong size (%v).\n\n",
			V, Subset, L, Subset.Len())
		return
	}

	if V.Len() != Subset.Len() {
		t.Errorf("TestGetSubsetBigger failed:\nV: %v\nSubset: %v\nL: %v\nSubset should be equal to V.\n\n",
			V, Subset, L)
		return
	}
}
