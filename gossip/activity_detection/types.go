package activity_detection

import (
	"ait_cyclon/gossip"
	"fmt"
	"os"
	"sort"
	"time"
)

type PayloadWithSets struct {
	Sender              gossip.Descriptor
	BeforeSet, AfterSet *gossip.View
	S                   *Sequence
}

type Payload struct {
	Sender gossip.Descriptor
	S      *Sequence
}

/******** Round ********/
type Round struct {
	D         gossip.Descriptor `json:"D"`
	StartTime time.Time         `json:"StartTime"`
}

func (r Round) String() string {
	return fmt.Sprintf("Round{D: %q, StartTime: %v}",
		r.D.ID(), r.StartTime)
}
func (r Round) Equals(s2 Round) bool {
	return r.D.Equals(s2.D) && r.StartTime.Equal(s2.StartTime) //&& r.EndTime.Equal(s2.EndTime)
}

/******** Sequence ********/
type Sequence map[time.Time]Round

func NewSequence(rounds ...Round) *Sequence {
	var S Sequence = make(map[time.Time]Round)
	S.Add(rounds...)
	return &S
}

func (S *Sequence) Add(rounds ...Round) {
	for _, r := range rounds {
		(*S)[r.StartTime] = r
	}
}
func (S *Sequence) Copy() *Sequence {
	var S2 Sequence = make(map[time.Time]Round)
	for key, item := range *S {
		S2[key] = item
	}
	return &S2
}
func (S *Sequence) Equals(S2 *Sequence) bool {
	if len(*S) != len(*S2) {
		return false
	}
	for _, r := range *S {
		// Maxime a perdu le JEU.
		// Je rentre dans pas longtemps sinon.
		if !S2.Has(r) {
			return false
		}
	}

	return true
}
func (S *Sequence) Get(t time.Time) Round {
	if r, ok := (*S)[t]; ok {
		return r
	}
	return Round{}
}

func (S *Sequence) GetDescriptors(exclude ...gossip.Descriptor) []gossip.Descriptor {
	V := gossip.NewView()
	for _, r := range *S {
		V.Add(r.D)
	}

	for _, d := range exclude {
		if d != nil {
			V.Remove(d)
		}
	}

	return V.GetDescriptors()
}

func (S *Sequence) GetLastRound() Round {
	sortedKeys := S.GetSortedKeys()
	lastTimestamp := sortedKeys[len(sortedKeys)-1]

	return (*S)[lastTimestamp]
}

func (S *Sequence) GetLastSubset(size int) *Sequence {
	keys := S.GetSortedKeys()
	if len(keys) > size {
		keys = keys[len(keys)-size:]
	} else {
		return S.Copy()
	}

	ret := NewSequence()
	for _, k := range keys {
		ret.Add((*S)[k])
	}
	return ret
}
func (S *Sequence) GetSortedKeys() []time.Time {
	keys := make([]time.Time, len(*S))
	i := 0
	for k := range *S {
		keys[i] = k
		i++
	}

	sort.Sort(timeSlice(keys))
	return keys

}
func (S *Sequence) Has(r Round) bool {
	_, ok := (*S)[r.StartTime]
	return ok
}
func (S *Sequence) Len() int {
	return len(*S)
}
func (S *Sequence) Merge(S2 *Sequence) bool {
	updated := false
	for k, v := range *S2 {
		if r, ok := (*S)[k]; ok && !r.Equals(v) {
			fmt.Fprintf(os.Stderr, "[Sequence.Merge] Sequence has different element at key %q:\n"+
				"\tIn S: %q; In S2: %q\n", k, r, v)
		} else if !ok {
			(*S)[k] = v
			updated = true
		}
	}

	return updated
}
func (S *Sequence) Remove(rounds ...Round) {
	for _, r := range rounds {
		delete(*S, r.StartTime)
	}
}
func (S *Sequence) String() string {
	ret := "Sequence{\n"
	for _, t := range S.GetSortedKeys() {
		ret += fmt.Sprintf("\tRound{%q, %q}\n",
			(*S)[t].D.ID(), (*S)[t].StartTime)
	}
	return ret + "}"
}

func SequenceDiff(S1, S2 *Sequence) *Sequence {
	S := S1.Copy()
	for key2 := range *S2 {
		delete(*S, key2)
	}

	return S
}

// Sort internal bullshit
type timeSlice []time.Time

func (t timeSlice) Len() int           { return len(t) }
func (t timeSlice) Less(i, j int) bool { return t[i].Before(t[j]) }
func (t timeSlice) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }

/******** DeviceSet ********/
// type DeviceSet map[gossip.Descriptor]struct{}
// func NewDeviceSet(devices ...gossip.Descriptor) DeviceSet {
// 	var ds DeviceSet = make(map[gossip.Descriptor]struct{})
// 	ds.Add(devices...)
// 	return ds
// }

// func (ds *DeviceSet) Add(devices ...gossip.Descriptor) {
// 	for _, d := range devices {
// 		(*ds)[d] = struct{}{}
// 	}
// }
// func (ds *DeviceSet) Remove(devices ...gossip.Descriptor) {
// 	for _, d := range devices {
// 		delete(*ds, d)
// 	}
// }
// func (ds *DeviceSet) Has(d gossip.Descriptor) bool {
// 	_, ok := (*ds)[d]
// 	return ok
// }
