package activity_detection

import (
	"ait_cyclon/gossip"
	"ait_cyclon/network"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	SELECT_BEFORE = 0
	SELECT_AFTER  = 1
	DEBUG_RAG     = false
	// MAX_SESSIONS_TO_SEND = 10
)

type ReactiveActivityGossiper struct {
	myself gossip.Descriptor

	sequence, initialSequence *Sequence
	otherSequences            struct {
		sync.RWMutex
		m map[string]*Sequence
	}

	// Number of peers to which we communicate our sequence updates
	fanout int

	// Network
	nm           network.Manager
	reqCh, ansCh <-chan network.Message

	// If ReactiveActivityGossiper is monitored,
	isMonitored bool
	// monitorLines contains the lines to send to monitoring
	monitorLines [][]string
	monitorMutex sync.Mutex
	// count #messages and # useless messages received
	nMessages, nUselessMessages int

	mutex sync.RWMutex
}

func NewReactiveActivityGossiper(Myself gossip.Descriptor, nm network.Manager,
	fanout int, jsonPath string, isMonitored bool) (*ReactiveActivityGossiper, error) {
	rag := new(ReactiveActivityGossiper)

	rag.nm = nm

	// Creation of the chans
	reqCh := make(chan network.Message)
	ansCh := make(chan network.Message)
	// Register chans on the network Manager
	rag.nm.RegisterObserver(network.MessageType("ACTIVITY_REQ"), reqCh)
	rag.nm.RegisterObserver(network.MessageType("ACTIVITY_ANS"), ansCh)
	// Save receive chan in ag object
	rag.reqCh = reqCh
	rag.ansCh = ansCh

	rag.myself = Myself
	rag.initialSequence = initSequence(jsonPath)
	rag.sequence = rag.initialSequence.Copy()

	rag.fanout = fanout

	// Initialise the otherSequences map:
	// for each descriptor in our S, we consider that each other node knows the same sequence
	rag.otherSequences.m = make(map[string]*Sequence)
	for _, d := range rag.sequence.GetDescriptors(rag.myself) {
		rag.otherSequences.m[d.ID()] = rag.initialSequence.Copy()
	}

	rag.isMonitored = isMonitored
	if rag.isMonitored {
		rag.addMonitorLine(time.Now(), rag.myself.ID())
	}

	debugRag("Reactive ActivityGossiper initialised.\n")

	return rag, nil
}

func (rag *ReactiveActivityGossiper) Loop() {
	debugRag("[ReactiveActivityGossiper.Loop] Loop started.\n")

	for {
		select {
		case mess := <-rag.reqCh:
			go rag.handleRequest(mess)
		case mess := <-rag.ansCh:
			go rag.handleAnswer(mess)
		}
	}
}

// ---------------------------------------
// ------- ActivityManager methods -------
// func (rag *ReactiveActivityGossiper) GetBeforeSet() []gossip.Descriptor {
// 	rag.mutex.RLock()
// 	defer rag.mutex.RUnlock()

// 	// Candidates are taken from the whole sequence
// 	cand := rag.sequence.GetDescriptors(rag.myself)

// 	return selection(gossip.NewView(cand...), rag.sequence, rag.myself,
// 		rag.setSize, SELECT_BEFORE).GetDescriptors()
// }

// func (rag *ReactiveActivityGossiper) GetAfterSet() []gossip.Descriptor {
// 	rag.mutex.RLock()
// 	defer rag.mutex.RUnlock()

// 	// Candidates are taken from the whole sequence
// 	cand := rag.sequence.GetDescriptors(rag.myself)

// 	return selection(gossip.NewView(cand...), rag.sequence, rag.myself,
// 		rag.setSize, SELECT_AFTER).GetDescriptors()
// }

func (rag *ReactiveActivityGossiper) AddRound(ts time.Time, deviceUsed gossip.Descriptor) {
	rag.mutex.Lock()

	newRound := Round{deviceUsed, ts}

	rag.sequence.Add(newRound)

	if rag.isMonitored {
		go rag.addMonitorLine(ts, rag.myself.ID())
	}

	rag.mutex.Unlock()

	debugRag("[ReactiveActivityGossiper.AddRound] Added a new round:\n%v\n\n", newRound)

	go rag.gossipUpdate(nil)
}

func (rag *ReactiveActivityGossiper) SelectPeers(n int) []gossip.Descriptor {
	rag.mutex.RLock()
	S := rag.sequence.Copy()
	myself := rag.myself
	rag.mutex.RUnlock()

	return SelectRoulettePeers(S, n, myself)

}

// ------- End of ActivityManager methods -------
// ----------------------------------------------

func (rag *ReactiveActivityGossiper) gossipUpdate(excluded gossip.Descriptor) {
	rag.mutex.RLock()
	defer rag.mutex.RUnlock()

	lastUsedDevice := rag.sequence.GetLastRound().D

	// We will communicate with nodes from our sequence
	// excluding myself, the device used in the last round, and the optional 'excluded' peer
	descs := rag.sequence.GetDescriptors(rag.myself, lastUsedDevice, excluded)
	// We choose 'fanout' random peers from
	fanout := rag.fanout
	if fanout > len(descs) {
		fanout = len(descs)
	}
	ids := rand.Perm(len(descs))[:fanout]
	// And fill peers with the descs with the picked ids
	peers := make([]gossip.Descriptor, fanout)
	for i, x := range ids {
		peers[i] = descs[x]
	}

	for _, peer := range peers {
		debugRag("[ReactiveActivityGossiper.gossipUpdate] Selected %q to send an ACTIVITY_REQ...\n",
			peer.ID())

		rag.otherSequences.Lock()

		if _, ok := rag.otherSequences.m[peer.ID()]; !ok {
			debugRag("[ReactiveActivityGossiper.gossipUpdate] Could not find %q in otherSequences.\n",
				peer.ID())
			rag.otherSequences.m[peer.ID()] = rag.initialSequence.Copy()
		}

		// diffSeq contains the rounds we believe peer does not know of among our sequence:
		// diffSeq = our Sequence \ peer's local Sequence
		diffSeq := SequenceDiff(rag.sequence, rag.otherSequences.m[peer.ID()])

		// We only communicate with peer if we have something for it
		if diffSeq.Len() > 0 {
			debugRag("[ReactiveActivityGossiper.gossipUpdate] Sending the following Sequence to %q:\n%v\n\n",
				peer.ID(), diffSeq)
			go gossip.EncodeAndSend(
				Payload{rag.myself, diffSeq},
				peer.GetAddr(), rag.nm, network.MessageType("ACTIVITY_REQ"))

			// Update our local view of peer's sequence
			// We are optimistic: we don't wait for any acknowledgment
			rag.otherSequences.m[peer.ID()].Merge(diffSeq)
		} else {
			debugRag("[ReactiveActivityGossiper.gossipUpdate] %q seems up to date, skpping it.\n",
				peer.ID())
		}

		rag.otherSequences.Unlock()
	}
}

func (rag *ReactiveActivityGossiper) handleRequest(mess network.Message) {
	x := gossip.Decode(mess, network.MessageType("ACTIVITY_REQ"))
	if x == nil {
		fmt.Fprintf(os.Stderr,
			"[ReactiveActivityGossiper.handleRequest] Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	debugRag("[ReactiveActivityGossiper.handleRequest] Received the following Sequence from %q's %s:\n%v\n\n",
		payload.Sender.ID(), mess.Type, payload.S)

	// Locking the otherSequences mutex while we use it
	rag.otherSequences.Lock()
	if _, ok := rag.otherSequences.m[payload.Sender.ID()]; !ok {
		rag.otherSequences.m[payload.Sender.ID()] = rag.initialSequence.Copy()
	}
	// We update our local version of the remote's sequence with what it sent
	rag.otherSequences.m[payload.Sender.ID()].Merge(payload.S)

	itsSeq := rag.otherSequences.m[payload.Sender.ID()]
	rag.otherSequences.Unlock()

	// Locking the rag Mutex while we modify/access the sequence
	rag.mutex.Lock()
	defer rag.mutex.Unlock()

	// We update our sequence with the received payload
	updatedMySequence := rag.sequence.Merge(payload.S)

	if rag.isMonitored {
		rag.nMessages++
		if !updatedMySequence {
			rag.nUselessMessages++
		} else {
			// If my sequence was updated and we monitor, add a monitor line
			go rag.addMonitorLine(time.Now(), payload.Sender.ID())
		}
	}

	// diffSeq contains the rounds known by us and not by the remote
	diffSeq := SequenceDiff(rag.sequence, itsSeq)

	// Only answer to the remote if we have rounds to send it
	if diffSeq.Len() > 0 {
		debugRag("[ReactiveActivityGossiper.handleRequest] Sending the following ACTIVITY_ANS Sequence to %q:\n%v\n\n",
			payload.Sender.ID(), diffSeq)
		go gossip.EncodeAndSend(
			Payload{rag.myself, diffSeq},
			payload.Sender.GetAddr(),
			rag.nm,
			network.MessageType("ACTIVITY_ANS"))
	} else {
		debugRag("[ReactiveActivityGossiper.handleRequest] Nothing to answer %q, we're up to date!\n\n",
			payload.Sender.ID())
	}

	// If my sequence was updated with this request, we forward the update to other peers
	// Excluding the remote that sent us the update
	if updatedMySequence {
		go rag.gossipUpdate(payload.Sender)
	}
}

func (rag *ReactiveActivityGossiper) handleAnswer(mess network.Message) {
	x := gossip.Decode(mess, network.MessageType("ACTIVITY_ANS"))
	if x == nil {
		fmt.Fprintf(os.Stderr,
			"[ReactiveActivityGossiper.handleAnswer] Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	debugRag("[ReactiveActivityGossiper.handleAnswer] Received the following Sequence from %q's %s:\n%v\n\n",
		payload.Sender.ID(), mess.Type, payload.S)

	// We update our local version of the remote's sequence with what it sent
	rag.otherSequences.Lock()
	if _, ok := rag.otherSequences.m[payload.Sender.ID()]; !ok {
		rag.otherSequences.m[payload.Sender.ID()] = rag.initialSequence.Copy()
	}
	rag.otherSequences.m[payload.Sender.ID()].Merge(payload.S)
	rag.otherSequences.Unlock()

	// Locking the mutex while we modify attributes
	rag.mutex.Lock()
	defer rag.mutex.Unlock()

	// We update our sequence with the received payload
	updatedMySequence := rag.sequence.Merge(payload.S)

	if rag.isMonitored {
		rag.nMessages++
		if !updatedMySequence {
			rag.nUselessMessages++
		} else {
			// If my sequence was updated and we monitor, add a monitor line
			go rag.addMonitorLine(time.Now(), payload.Sender.ID())
		}
	}
}

func (rag *ReactiveActivityGossiper) addMonitorLine(ts time.Time, updateSrcID string) {
	// //if rag.isMonitored {
	// // Beware: these two functions call the rag.mutex
	// beforeSet := rag.GetBeforeSet()
	// afterSet := rag.GetAfterSet()

	// beforeStr, afterStr := "", ""
	// for i, d := range beforeSet {
	// 	beforeStr += d.ID()
	// 	if i < len(beforeSet)-1 {
	// 		beforeStr += gossip.ID_SEPARATOR
	// 	}
	// }
	// for i, d := range afterSet {
	// 	afterStr += d.ID()
	// 	if i < len(afterSet)-1 {
	// 		afterStr += gossip.ID_SEPARATOR
	// 	}
	// }

	rag.mutex.Lock()
	S := ""
	timestamps := rag.sequence.GetSortedKeys()
	for i, k := range timestamps {
		S += k.Format(TIME_LAYOUT)
		if i != len(timestamps)-1 {
			S += gossip.ID_SEPARATOR
		}
	}

	nMessages, nUselessMessages := rag.nMessages, rag.nUselessMessages
	rag.nMessages, rag.nUselessMessages = 0, 0
	rag.mutex.Unlock()

	rag.monitorMutex.Lock()
	rag.monitorLines = append(rag.monitorLines,
		[]string{
			rag.myself.ID(),        // My ID
			rag.myself.GetAddr(),   // My address
			ts.Format(TIME_LAYOUT), // time when we received update or the last round's timestamp
			S,
			updateSrcID,                    // Device that sent us the update
			strconv.Itoa(nMessages),        // number of received messages since last line
			strconv.Itoa(nUselessMessages), // number of received useless messages since last line
		})
	rag.monitorMutex.Unlock()
}

func debugRag(format string, a ...interface{}) {
	if DEBUG_RAG {
		fmt.Printf(format, a...)
	}
}
