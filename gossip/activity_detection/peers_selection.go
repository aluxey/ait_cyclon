package activity_detection

import (
	"ait_cyclon/gossip"
	"fmt"
	"math/rand"
	//"io/ioutil"
	//"os"
)

func GetPeersScore(S *Sequence,
	sourceDevice gossip.Descriptor) (map[string]int, map[string]gossip.Descriptor) {

	// fmt.Printf("[SelectRoulettePeers] Source device:\n%v\n", sourceDevice)
	// fmt.Printf("[SelectRoulettePeers] Exclude set:\n%v\n", exclude)

	// Score: count of times each device was used right after our device
	peersScore := make(map[string]int)
	peers := make(map[string]gossip.Descriptor)
	Skeys := S.GetSortedKeys()

	for i := 0; i < len(Skeys)-1; i++ {
		Di := S.Get(Skeys[i]).D
		Dnext := S.Get(Skeys[i+1]).D
		if Di.Equals(sourceDevice) && !Dnext.Equals(sourceDevice) {

			// Skip descriptors in exclude set
			// if exclude != nil && isInArray(exclude, S.Get(Skeys[i+1]).D) {
			// 	continue
			// }

			peers[Dnext.ID()] = Dnext

			if _, ok := peersScore[Dnext.ID()]; !ok {
				peersScore[Dnext.ID()] = 1
			} else {
				peersScore[Dnext.ID()] += 1
			}
		}
	}

	// Skip offline peers if checkDevicesState
	// nOffline := 0
	// if checkDevicesState {
	// 	for p := range peersScore {
	// 		if !isOnlinePeer(devicesStatePath, p.ID()) {
	// 			nOffline += 1
	// 			delete(peersScore, p)
	// 		}
	// 	}
	// }

	//return peersScore, nOffline
	return peersScore, peers
}

// Roulette-wheel selection via stochastic acceptance (cf https://arxiv.org/abs/1109.3627)
// Does not need myself in exclude
func SelectRoulettePeers(S *Sequence, fanout int,
	sourceDevice gossip.Descriptor) []gossip.Descriptor {

	fmt.Printf("[SelectRoulettePeers] SourceDevice: %v\n", sourceDevice)

	// peersScore contains one entry per ONLINE device that was ALREADY USED
	peersScore, peers := GetPeersScore(S,
		/*checkDevicesState, devicesStatePath,*/
		sourceDevice /*, exclude...*/)

	fmt.Printf("[SelectRoulettePeers] PeersScore:\n")
	for dID, v := range peersScore {
		fmt.Printf("\t%s: %d\n", dID, v)
	}

	selectedPeers := make([]gossip.Descriptor, 0, fanout)

	if len(peersScore) == 0 {
		// I peersScore is too small (less that two), return random peers
		ret := SelectRandomPeers(S, fanout) /*checkDevicesState, devicesStatePath,
		exclude...*/
		fmt.Printf("[SelectRoulettePeers] Returning following RANDOM peers:\n%v\n\n", ret)
		return ret
	} else if len(peersScore) <= fanout {
		for _, p := range peers {
			selectedPeers = append(selectedPeers, p)
		}
		return selectedPeers
	}

	for len(selectedPeers) < fanout {
		var p gossip.Descriptor

		// Getting the maximum score
		maxScore := 0
		for _, score := range peersScore {
			if maxScore < score {
				maxScore = score
			}
		}

		accepted := false
		// We randomly pick a node p from peersScore, and accept it w/ proba of peersScore[p]/maxScore
		for !accepted {
			// Crappy uniform random selection in a map:
			// In the end, p contains a random peer from the map
			randomIndex := rand.Intn(len(peersScore))
			for _, p = range peers {
				if randomIndex == 0 {
					break
				}
				randomIndex--
			}

			// Random draw to accept p as selected with a proba of peersScore[p]/maxScore
			accepted = rand.Float32() <= float32(peersScore[p.ID()])/float32(maxScore)
		}

		selectedPeers = append(selectedPeers, p)
		// We only accept each peer once: delete p from peersScore (and we will recompute maxScore above)
		delete(peersScore, p.ID())
		delete(peers, p.ID())
	}

	fmt.Printf("[SelectRoulettePeers] Returning following peers:\n%v\n\n", selectedPeers)

	return selectedPeers
}

func SelectRandomPeers(S *Sequence, fanout int) []gossip.Descriptor {

	descs := S.GetDescriptors()

	// We choose 'fanout' random peers from
	if fanout > len(descs) {
		fanout = len(descs)
	}
	//ids := rand.Perm(len(descs))[:fanout]

	fmt.Printf("[SelectRandomPeers] Available descriptors:\n%v\n", descs)

	// And fill peers with the descs with the picked ids
	//peers := make([]Descriptor, fanout)
	peers := make([]gossip.Descriptor, 0)
	for _, x := range rand.Perm(len(descs)) {
		// Skip offline peers if checkDevicesState
		// if checkDevicesState &&
		// 	!isOnlinePeer(devicesStatePath, descs[x].ID()) {
		// 	continue
		// }

		peers = append(peers, descs[x])

		if len(peers) == fanout {
			break
		}
	}

	fmt.Printf("[SelectRandomPeers] Selected peers:\n%v\n", peers)

	return peers
}

// ------------ Utils ------------

// Little linear search function
func isInArray(arr []gossip.Descriptor, desc gossip.Descriptor) bool {
	for _, d := range arr {
		if d == nil {
			continue
		}
		if desc.Equals(d) {
			return true
		}
	}
	return false
}

// Check device's state in status file
// func isOnlinePeer(path, deviceID string) bool {
// 	if v, err := ioutil.ReadFile(path + deviceID); err != nil {
// 		fmt.Fprintf(os.Stderr, "[isOnlinePeer] Failed opening %s: %v.\n", path+deviceID, err)
// 		return false
// 	} else {
// 		return string(v[:]) == "1\n"
// 	}
// }
