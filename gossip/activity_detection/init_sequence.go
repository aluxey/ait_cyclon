package activity_detection

import (
	"ait_cyclon/gossip"
	"ait_cyclon/network"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

type RoundJson struct {
	Name      string `json:"Name"`
	Addr      string `json:"Addr"`
	StartTime string `json:"StartTime"`
}

const (
	TIME_LAYOUT = "2006-01-02 15:04:05.000000"
)

type SequenceJson []RoundJson

func initSequence(jsonPath string) *Sequence {
	sequence := NewSequence()

	if jsonPath == "" {
		return sequence
	}

	bytes, err := ioutil.ReadFile(jsonPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[ActivityGossiper.initSequence] Failed opening %v: %v\n",
			jsonPath, err)
		return sequence
	}

	var seq SequenceJson
	if err = json.Unmarshal(bytes, &seq); err != nil {
		fmt.Fprintf(os.Stderr, "[ActivityGossiper.initSequence] Failed unmarshaling JSON file: %v\n",
			err)
		return sequence
	}

	for _, roundJson := range seq {
		var t time.Time
		if t, err = time.Parse(TIME_LAYOUT, roundJson.StartTime); err != nil {
			fmt.Fprintf(os.Stderr, "[ActivityGossiper.initSequence] Failed decoding time %q: %v\n",
				roundJson.StartTime, err)
			continue
		}

		sequence.Add(Round{
			D: gossip.NewBaseDescriptor(
				roundJson.Name,
				roundJson.Addr+":"+network.UDP_PORT),
			StartTime: t,
		})
	}

	fmt.Printf("[ActivityGossiper.initSequence] We successfully extracted the initial sequence:\n%v\n", sequence)

	return sequence
}
