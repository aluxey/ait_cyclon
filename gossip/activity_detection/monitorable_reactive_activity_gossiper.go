package activity_detection

import (
	//"ait_cyclon/gossip"
	"fmt"
	"time"
)

func (rag *ReactiveActivityGossiper) GetFilename() string {
	now := time.Now()
	return fmt.Sprintf("activity_gossiper-%v_%02d-%02d-%02d_%02dh%02d.csv",
		rag.myself.ID(),
		now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
}

func (rag *ReactiveActivityGossiper) GetHeader() []string {
	return []string{"ID", "Address", "Timestamp",
		"SequenceTimestamps", "UpdateSource", "nMessagesReceived", "nUselessMessagesReceived"}
}

func (rag *ReactiveActivityGossiper) GetLine() ([]string, bool, error) {
	rag.monitorMutex.Lock()
	defer rag.monitorMutex.Unlock()

	if len(rag.monitorLines) == 0 {
		// Monitor won't write empty lines
		return []string{}, false, nil
	}

	// We pop oldest element first
	ret := rag.monitorLines[0]
	// And remove it from monitorLines
	rag.monitorLines = rag.monitorLines[1:]

	// returns the line, and set "hasMore" boolean if monitorLines is not empty
	return ret, (len(rag.monitorLines) > 0), nil
}

func (rag *ReactiveActivityGossiper) IsRunning() bool { return true }
