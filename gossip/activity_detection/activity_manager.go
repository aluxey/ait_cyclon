package activity_detection

import (
	"ait_cyclon/gossip"
	"time"
)

// This interface abstracts our activity gossipers
// It is used by the Session Manager
type ActivityManager interface {
	AddRound(ts time.Time, deviceUsed gossip.Descriptor)
	SelectPeers(n int) []gossip.Descriptor

	// GetBeforeSet() []gossip.Descriptor
	// GetAfterSet() []gossip.Descriptor
}
