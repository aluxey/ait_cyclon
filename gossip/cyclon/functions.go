package cyclon

import (
	"ait_cyclon/gossip"
	"fmt"
	//"os"
)

// Nul, il faut utiliser GO's logger, ce genre de chose bien intégrée
func debug(format string, a ...interface{}) {
	if DEBUG {
		fmt.Printf(format, a...)
	}
}

// gossip.View manipulation functions used by Cyclon only:
func getViewSubset(v *gossip.View, length int) *gossip.View {
	if len(*v) < length {
		return v.Copy()
	}

	subset := gossip.NewView(v.GetDescriptors()[:length]...)

	// var subset gossip.View = make(map[string]gossip.Descriptor)
	// i := 0

	// iteration on map has random order
	// for _, item := range *v {
	// 	if i >= length {
	// 		break
	// 	}
	// 	//subset[key] = item
	// 	subset.Add(item)
	// 	i++
	// }
	return subset
}

// mergeAmongReplaceable: Merge v2 into v up to maxLength items, romoving entries among repl to make room for v2's items.
// v: pointer to View, the View we are replacing into
// v2: pointer to View, the View we are replacing from
// repl: pointer to View, the items in v available for replacement
// maxLength: the desired final length of v
func mergeAmongReplaceable(v *gossip.View, v2 *gossip.View, repl *gossip.View, maxLength int) {
	// First make room into the output view by removing the replaceable elements
	v.Diff(repl)

	// Then add items from the input view v2, and replace elements existing in v if v2's are younger
	for _, item := range *v2 {
		if len(*v) >= maxLength {
			break
		}

		if viewItem, ok := (*v)[item.GetAddr()]; ok && viewItem.Younger(item) {
			continue
		}
		(*v)[item.GetAddr()] = item.Copy()
	}
	// Finally, fill remaining place in v with items from repl (without replacement)
	for _, item := range *repl {
		if len(*v) >= maxLength {
			break
		}

		if _, ok := (*v)[item.GetAddr()]; ok {
			continue
		}
		(*v)[item.GetAddr()] = item.Copy()
	}

	// for _, v2item := range *v2 {
	// 	// While we have some room left in the view, just append items to v
	// 	if v.Len() < maxLength {
	// 		v.Add(v2item)
	// 		continue
	// 	}

	// 	// If v already contains v2item, we keep the "best" (according to Desc: oldest)
	// 	// Should not happen, though, since entries already in view gets removed
	// 	// in CY.MergeWithView before calling this function
	// 	if v.Has(v2item) {
	// 		v.Add(v2item)
	// 		continue
	// 	}

	// 	// If repl is empty, we return before having merged v and v2
	// 	if repl.Len() == 0 {
	// 		fmt.Fprintf(os.Stderr, "[view.MergeAmongRepl] We emptied repl "+
	// 			"before we finished merging v and v2 (%v items remain).", v2.Len())
	// 		return
	// 	}

	// 	// In the last case, we remove an element from repl in v (unless v has it)
	// 	// And add v2item to v
	// 	for repl.Len() > 0 {
	// 		toRemove := repl.Pop()
	// 		if v.Has(toRemove) {
	// 			v.Remove(toRemove)
	// 			v.Add(v2item)
	// 			break
	// 		}
	// 	}
	// }
}
