package cyclon

import (
	"ait_cyclon/gossip"
	"ait_cyclon/monitoring/shiviz"
	"ait_cyclon/network"
	"fmt"
	"os"
	"sync"
	"time"
)

const (
	DEBUG         = false
	CHAN_BUF_SIZE = 5
)

// Observer pattern to track answers
type Cyclon struct {
	gossip.GossipAlgorithm // Embedding Gossip base type

	answerObservers struct {
		sync.RWMutex
		m map[string]chan gossip.Payload
	}

	bs gossip.Bootstrapper

	running bool

	// We use a map to avoid duplicate entries
	viewReceivers map[ViewReceiver]struct{}
}

func Create(Myself gossip.Descriptor, bs gossip.Bootstrapper, nm network.Manager,
	period, viewSize, gossipSize int) (*Cyclon, error) {

	// First kill Cyclon if it existed
	cy = new(Cyclon)

	var err error
	if cy.GossipAlgorithm, err = gossip.NewGossipAlgorithm(Myself, nm,
		period, viewSize, gossipSize); err != nil {
		return nil, err
	}

	// Creation of the chans
	reqCh := make(chan network.Message)
	ansCh := make(chan network.Message)
	// Register chans on the network Manager
	cy.Nm.RegisterObserver(network.MessageType("CYCLON_REQ"), reqCh)
	cy.Nm.RegisterObserver(network.MessageType("CYCLON_ANS"), ansCh)
	// Save receive chan in cy object
	cy.ReqCh = reqCh
	cy.AnsCh = ansCh

	cy.answerObservers.m = make(map[string]chan gossip.Payload)
	cy.viewReceivers = make(map[ViewReceiver]struct{})

	cy.bs = bs

	cy.running = false

	fmt.Printf("Cyclon started with viewSize=%d, gossipSize=%d and a period of %dms.\n",
		viewSize, gossipSize, period)

	return cy, nil
}

func (cy *Cyclon) Loop() {
	debug("[cyclon.Loop] Cyclon's loop started.\n")

	cy.StopCh = make(chan struct{})
	cy.running = true

	ticker := time.NewTicker(cy.Period)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			go cy.active()
		case dtg := <-cy.ReqCh:
			go cy.passive(dtg)
		case dtg := <-cy.AnsCh:
			go cy.dispatchAnswers(dtg)
		case <-cy.StopCh:
			break
		}
	}
}

func (cy *Cyclon) Stop() {
	// handles closing cy.StopCh
	cy.GossipAlgorithm.Stop()
	cy.running = false
}

func (cy *Cyclon) SetMyself(myself gossip.Descriptor) {
	cy.Mutex.Lock()
	defer cy.Mutex.Unlock()

	cy.Myself = myself
}

// Part of the monitoring.Monitorable interface
func (cy *Cyclon) IsRunning() bool { return cy.running }

func (cy *Cyclon) active() {
	// We lock mutex while we use V or Myself
	cy.Mutex.Lock()
	// We create a local variable in case the counter gets incremented while out of mutex
	cnt := cy.CntActive
	cy.CntActive++

	// 1 - Increase by one the age of all neighbors
	cy.V.IncrementAge()

	// 2 - Select neighbor Q with the highest age among all neighbors ...
	var q gossip.Descriptor
	if cy.V.IsEmpty() {
		debug("[cyclon.ActiveAgent #%v] Our view is empty, we query the bootstrap server.\n",
			cnt)
		if addr, err := cy.bs.GetAddress(); err != nil {
			fmt.Fprintf(os.Stderr,
				"[cyclon.ActiveAgent #%v] The bootstraper failed providing an address: %v\n",
				cnt, err)
			cy.Mutex.Unlock()
			return
		} else {
			debug("[cyclon.ActiveAgent #%v] The bootstraper sent us the following address: %v\n",
				cnt, addr)
			// q won't be added to Cyclon's view
			// We use the BaseDescriptor to create a gossip.Descriptor without other knowledge than the Addr
			q = gossip.NewBaseDescriptor(addr)
		}
	} else {
		q = cy.V.GetOldest()
		cy.V.Remove(q)
	}

	// 2 - Select [...] l-1 other random neighbors
	buf_snd := getViewSubset(cy.V, cy.GOSSIP_SIZE-1)
	// 3 - Replace Q's entry w/ a new entry of age 0 and with P's address
	// That is: add my descriptor (has age of 0) to the view we will send
	buf_snd.Remove(q)
	buf_snd.Add(cy.Myself)

	debug("[cyclon.ActiveAgent #%v] Sending following CYCLON_REQ to %s:\n%v\n",
		cnt, q, buf_snd)

	// 4 - Send the updated subset to Q
	go gossip.EncodeAndSend(gossip.Payload{cy.Myself, buf_snd.Copy()},
		q.GetAddr(), cy.Nm, network.MessageType("CYCLON_REQ"))

	cy.Mutex.Unlock()

	timeout := time.After(cy.ConnectionTimeout)
	select {
	case <-timeout:
		fmt.Fprintf(os.Stderr,
			"[cyclon.ActiveAgent #%v] %v did not answer before timeout.\n",
			cnt, q.GetAddr())
		cy.Mutex.Lock()
		cy.V.Remove(q)
		shiviz.WriteLocal("CYCLON_REQ to "+q.GetAddr()+" timed out", cy.V.GetIDs())
		cy.Mutex.Unlock()
		return
	// 5 - Receive from Q a subset of no more than l of its own entries
	case payload := <-cy.getChannel(q.GetAddr()):
		// if payload == nil {
		// 	fmt.Fprintf(os.Stderr,
		// 		"[cyclon.ActiveAgent #%v] Failed decoding payload.\n",
		// 		cnt)
		// 	return
		// }

		debug("[cyclon.ActiveAgent #%v] Received following CYCLON_ANS from %s:\n%v\n\n",
			cnt, payload.Sender.GetAddr(), payload.V)

		cy.mergeWithView(payload, buf_snd, q)
	}
}

func (cy *Cyclon) passive(dtg network.Message) {
	x := gossip.Decode(dtg, network.MessageType("CYCLON_REQ"))

	cy.Mutex.Lock()
	cy.CntPassive++

	if x == nil {
		fmt.Fprintf(os.Stderr, "[cyclon.Passive #%v] The received payload is nil",
			cy.CntPassive)
		cy.Mutex.Unlock()
		return
	}
	payload := x.(gossip.Payload)

	debug("[cyclon.Passive] Received following CYCLON_REQ from %s:\n%v\n",
		payload.Sender, payload.V)

	// Upon reception, Q selects l of its own neighbors to send back to P
	buf_snd := getViewSubset(cy.V, cy.GOSSIP_SIZE)
	//buf_snd.Add(cy.Myself)
	// We remove P from the view (in paper at step 6, but doing so here
	// removes useless network overload)
	buf_snd.Remove(payload.Sender)

	debug("[cyclon.Passive] Sending following CYCLON_ANS to %s:\n%v\n\n",
		payload.Sender, buf_snd)

	go gossip.EncodeAndSend(gossip.Payload{cy.Myself, buf_snd.Copy()},
		payload.Sender.GetAddr(), cy.Nm, network.MessageType("CYCLON_ANS"))

	cy.Mutex.Unlock()

	cy.mergeWithView(payload, buf_snd, nil)
}

func (cy *Cyclon) dispatchAnswers(dtg network.Message) {
	x := gossip.Decode(dtg, network.MessageType("CYCLON_ANS"))
	if x == nil {
		fmt.Fprintf(os.Stderr,
			"[cyclon.dispatchAnswers] Failed decoding payload.\n")
		return
	}
	payload := x.(gossip.Payload)

	addr := payload.Sender.GetAddr()

	cy.answerObservers.RLock()
	if cy.answerObservers.m[addr] == nil {
		fmt.Fprintf(os.Stderr, "[cyclon.DispatchAnswers] Received answer from unexpected sender %v, skipping.\n", addr)
		cy.answerObservers.RUnlock()
		return
	}
	//debug("[cyclon.dispatchAnswers] Sending answer received from %v to channel.\n", addr)

	// non-blocking -channel has buffer of CHAN_BUF_SIZE
	cy.answerObservers.m[addr] <- payload
	cy.answerObservers.RUnlock()
}

func (cy *Cyclon) mergeWithView(payload gossip.Payload, repl *gossip.View, q gossip.Descriptor) {
	// 5 - Receive FROM Q ...
	if q != nil && q.GetAddr() != payload.Sender.GetAddr() {
		fmt.Fprintf(os.Stderr,
			"[cyclon.mergeWithView] We expected a datagram from %v, but it came from %v!",
			q.GetAddr(), payload.Sender.GetAddr())
		return
	}

	// 5 - ... a subset of NO MORE THAN L of its own entries
	v := payload.V
	if v.Len() > cy.GOSSIP_SIZE {
		fmt.Fprintf(os.Stderr,
			"[cyclon.mergeWithView] The received view (len: %v) is bigger than gossip size (%v), it shouldn't be!\n",
			v.Len(), cy.GOSSIP_SIZE)
		return
	}

	cy.Mutex.Lock()
	// 6 - Discard entries pointing at P ...
	//v.Remove(cy.Myself) // Done in passive, before sending
	// ... and entries already contained in P's cache
	v.Diff(cy.V)

	// Save previous state of our View
	prevView := cy.V.Copy()

	// 7 - Update P's cache to include all remaining entries,
	// by firstly using empty cache slots (if any),
	// and secondly replacing entries among the ones sent to Q
	mergeAmongReplaceable(cy.V, v, repl, cy.VIEW_SIZE)

	// Cyclon sometimes sends Myself... What did I do? :'( (17-05-17)
	cy.V.Remove(cy.Myself)

	// If the view has changed, send the new one to the ViewReceivers
	cy.updateViewReceivers(prevView)

	shiviz.WriteLocal("Updated CYCLON view", cy.V.GetIDs())
	debug("[cyclon.mergeWithView] Successfully merged! New view is:\n%v\n\n", cy.V)
	cy.Mutex.Unlock()
}

func (cy *Cyclon) getChannel(Addr string) <-chan gossip.Payload {
	cy.answerObservers.Lock()
	defer cy.answerObservers.Unlock()

	if cy.answerObservers.m[Addr] == nil {
		debug("[cyclon.getChannel] Registering new channel for %v.\n", Addr)
		cy.answerObservers.m[Addr] = make(chan gossip.Payload, CHAN_BUF_SIZE)
	}

	return cy.answerObservers.m[Addr]
}

// Methods of shiviz.GossipAlgorithm interface
// Print the list of profiles in the view (for Monitoring)
func (cy *Cyclon) PrintName() string { return "cyclon" }
