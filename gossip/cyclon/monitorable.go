package cyclon

import (
	"fmt"
	"time"
)

// Implement the methods to satisfy the monitoring/monitorable.go "Monitorable" interface

func (cy *Cyclon) GetFilename() string {
	cy.Mutex.RLock()
	defer cy.Mutex.RUnlock()

	now := time.Now()
	return fmt.Sprintf("cyclon-%v_%02d-%02d-%02d_%02dh%02d.csv",
		cy.Myself.GetAddr(),
		now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
}

func (cy *Cyclon) GetHeader() []string {
	return []string{"Profile", "Timestamp", "View"}
}

func (cy *Cyclon) GetLine() ([]string, bool, error) {
	cy.Mutex.RLock()
	defer cy.Mutex.RUnlock()

	if !cy.IsRunning() {
		return []string{}, false, fmt.Errorf("Cyclon is not running.")
	}

	if cy.Myself == nil {
		return []string{}, false, fmt.Errorf("Cyclon has no descriptor for itself.")
	}
	return []string{cy.Myself.GetAddr(), time.Now().Format(time.RFC3339), cy.V.GetIDs()}, false, nil
}

// IsRunning defined in cyclon.go
