package cyclon

import (
	"fmt"
)

// Cyclon singleton pointer
var cy *Cyclon

func GetCyclon() (*Cyclon, error) {
	if cy == nil {
		return nil, fmt.Errorf("[GetCyclon] Error: Cyclon singleton must be created first.")
	}
	return cy, nil
}
