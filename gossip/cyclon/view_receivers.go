package cyclon

import "ait_cyclon/gossip"

/*
ViewReceivers implement the ReceiveViewUpdate(*View) method and register to Cyclon.

Cyclon pushes its view every time it is updated, by calling its updateViewReceivers(*View) method.
The calling function needs to provide the previous view as a parameter for comparison before sending updates.
*/

type ViewReceiver interface {
	// Send a copy!
	ReceiveViewUpdate(*gossip.View)
}

func (cy *Cyclon) AddViewReceiver(vr ViewReceiver) {
	// cy.viewReceivers modifications must be done inside the mutex
	cy.Mutex.Lock()
	defer cy.Mutex.Unlock()

	if _, ok := cy.viewReceivers[vr]; ok {
		debug("[cy.AddViewReceiver] This ViewReceiver was already added to Cyclon: %v\n", vr)
		return
	}

	cy.viewReceivers[vr] = struct{}{}
}

// This may be useful in the future, but not now
//func (cy *Cyclon) RemoveViewReceiver(vr ViewReceiver) {}

func (cy *Cyclon) updateViewReceivers(prevView *gossip.View) {
	// Must be called from within the mutex!

	if prevView.Equals(cy.V) {
		return
	}

	debug("[cy.updateViewReceivers] The view has changed: " +
		"we update the ViewReceivers.\n")

	for vr := range cy.viewReceivers {
		go vr.ReceiveViewUpdate(cy.V.Copy())
	}
}
