#!/bin/bash

for id in `cat .containers_id`; do
	echo "-------- "$id" --------"
	echo "getConnection:"
	docker logs $id 2>&1 | rg "getConnection"
	echo "receiveDaemon:"
	docker logs $id 2>&1 | rg "receiveDaemon"
	echo "bind:"
	docker logs $id 2>&1 | rg "bind"
	echo
done
