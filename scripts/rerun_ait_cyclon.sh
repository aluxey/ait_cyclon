#!/bin/bash

N_CONTAINERS=12
EXPERIMENT_TIME=120 # seconds
MONITOR=true
STOP_NOT_RM=1 # Stop containers instead of rm'ing them (to access docker logs)
CYCLON_PERIOD=1000 # milliseconds
# Activity Detection
AD_PERIOD=2000 # milliseconds 
SESSION_PERIOD=2000 # milliseconds 
AD_VIEW_SIZE=2
AD_GOSSIP_SIZE=2

CONTAINERS_ID_FILE=".containers_id"
CONTAINERS_IP_FILE=".containers_ip"
cat /dev/null > $CONTAINERS_ID_FILE
cat /dev/null > $CONTAINERS_IP_FILE

PROJECT_DIR=$(pwd)
MONITOR_DIR=$PROJECT_DIR/monitoring_output
ARGS="-net=13.37.0.0/16 -m=/monitoring_output -cy_period=$CYCLON_PERIOD "
ARGS=$ARGS"-ad_period=$AD_PERIOD -ad_viewsize=$AD_VIEW_SIZE -ad_gossipsize=$AD_GOSSIP_SIZE -session_period=$SESSION_PERIOD"
if [ $MONITOR = true ]; then 
	ARGS=$ARGS"-mnet -msession -mactivity "
fi
ARGS=$ARGS"-bs_addr="
GOSSIP_PORT=10337

if [ -d $MONITOR_DIR ]; then 
	rm -f $MONITOR_DIR/*
else 
	if [ $MONITOR = true ]; then 
		mkdir $MONITOR_DIR
	fi
fi
echo -n "Removing previous instances... "
docker rm -f $(docker ps -aq -f "ancestor=ait_cyclon_image") > /dev/null 2>&1
echo "Done."

$PROJECT_DIR/scripts/build_ait_cyclon.sh
if [ $? -ne 0 ]; then
	exit 1
fi

docker network create --subnet=13.37.0.0/20 overlay_net > /dev/null 2>&1 

for i in `seq 1 $N_CONTAINERS`; do
	echo "Starting container #$i"
	# Sourcing notation (". <script>") to get $container_id and $container_ip to this file
	. $PROJECT_DIR/scripts/run_ait_cyclon.sh $ARGS

	if [ $i -eq 1 ]; then
		ARGS="$ARGS$container_ip:$GOSSIP_PORT"
	fi

	echo $container_id >> $CONTAINERS_ID_FILE
	echo $container_ip >> $CONTAINERS_IP_FILE
done


echo "Instances are running, we wait $EXPERIMENT_TIME seconds"
sleep $EXPERIMENT_TIME


if [ $STOP_NOT_RM -eq 0 ]; then
	echo "Deleting instances"
	docker rm -f $(docker ps -aq -f "ancestor=ait_cyclon_image") > /dev/null 2>&1
else
	echo "Stopping instances"
	docker stop $(docker ps -aq -f "ancestor=ait_cyclon_image") > /dev/null 2>&1 
fi
